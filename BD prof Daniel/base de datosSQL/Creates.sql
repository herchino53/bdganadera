

CREATE DATABASE IF NOT EXISTS `granjavacuna`;
USE `granjavacuna`;

#
# Table structure for table 'Acciones'
#

DROP TABLE IF EXISTS `Acciones`;

CREATE TABLE `Acciones` (
  `CodAccion` INTEGER NOT NULL, 
  `Accion` VARCHAR(50)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'AccNivel'
#

DROP TABLE IF EXISTS `AccNivel`;

CREATE TABLE `AccNivel` (
  `CodAccion` INTEGER NOT NULL, 
  `Nivel` INTEGER NOT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'CalculoEstadistico'
#

DROP TABLE IF EXISTS `CalculoEstadistico`;

CREATE TABLE `CalculoEstadistico` (
  `CodHa` VARCHAR(12) NOT NULL, 
  `NombreVariable` VARCHAR(25) NOT NULL, 
  `NoPartoLactancia` INTEGER, 
  `N` INTEGER, 
  `X` DOUBLE NULL, 
  `D` DOUBLE NULL, 
  `N1` INTEGER, 
  `X1` DOUBLE NULL, 
  `D1` DOUBLE NULL, 
  `N2` INTEGER, 
  `X2` DOUBLE NULL, 
  `D2` DOUBLE NULL, 
  `UnicoPadre` VARCHAR(12), 
  `Raza` VARCHAR(4)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Chequeados_Temporal'
#

DROP TABLE IF EXISTS `Chequeados_Temporal`;

CREATE TABLE `Chequeados_Temporal` (
  `ID` INTEGER DEFAULT 0, 
  `Chequeado` TINYINT(1) DEFAULT 1
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'CierreInventario'
#

DROP TABLE IF EXISTS `CierreInventario`;

CREATE TABLE `CierreInventario` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `IdCierre` INTEGER NOT NULL AUTO_INCREMENT, 
  `TipoCierre` VARCHAR(1) NOT NULL, 
  `FechaCierre` DATETIME NOT NULL, 
  `VacasMan` INTEGER, 
  `NovillasMan` INTEGER, 
  `MautasMan` INTEGER, 
  `BecerrasMan` INTEGER, 
  `TorosMan` INTEGER, 
  `NovillosMan` INTEGER, 
  `MautesMan` INTEGER, 
  `BecerrosMan` INTEGER, 
  `VacasIn` INTEGER, 
  `NovillasIn` INTEGER, 
  `MautasIn` INTEGER, 
  `BecerrasIn` INTEGER, 
  `TorosIn` INTEGER, 
  `NovillosIn` INTEGER, 
  `MautesIn` INTEGER, 
  `BecerrosIn` INTEGER, 
  `MVacas` INTEGER, 
  `MNovillas` INTEGER, 
  `MMautas` INTEGER, 
  `MBecerras` INTEGER, 
  `MToros` INTEGER, 
  `MNovillos` INTEGER, 
  `MMautes` INTEGER, 
  `MBecerros` INTEGER, 
  `VVacas` INTEGER, 
  `VNovillas` INTEGER, 
  `VMautas` INTEGER, 
  `VBecerras` INTEGER, 
  `VToros` INTEGER, 
  `VNovillos` INTEGER, 
  `VMautes` INTEGER, 
  `VBecerros` INTEGER, 
  `SVacas` INTEGER, 
  `SNovillas` INTEGER, 
  `SMautas` INTEGER, 
  `SBecerras` INTEGER, 
  `SToros` INTEGER, 
  `SNovillos` INTEGER, 
  `SMautes` INTEGER, 
  `SBecerros` INTEGER, 
  `CVacas` INTEGER, 
  `CNovillas` INTEGER, 
  `CMautas` INTEGER, 
  `CBecerras` INTEGER, 
  `CToros` INTEGER, 
  `CNovillos` INTEGER, 
  `CMautes` INTEGER, 
  `CBecerros` INTEGER, 
  `RVacas` INTEGER, 
  `RNovillas` INTEGER, 
  `RMautas` INTEGER, 
  `RBecerras` INTEGER, 
  `RToros` INTEGER, 
  `RNovillos` INTEGER, 
  `RMautes` INTEGER, 
  `RBecerros` INTEGER, 
  `DVacas` INTEGER, 
  `DNovillas` INTEGER, 
  `DMautas` INTEGER, 
  `DBecerras` INTEGER, 
  `DToros` INTEGER, 
  `DNovillos` INTEGER, 
  `DMautes` INTEGER, 
  `DBecerros` INTEGER, 
  `ANVacas` INTEGER, 
  `ANNovillas` INTEGER, 
  `ANMautas` INTEGER, 
  `ANBecerras` INTEGER, 
  `ANToros` INTEGER, 
  `ANNovillos` INTEGER, 
  `ANMautes` INTEGER, 
  `ANBecerros` INTEGER, 
  `APVacas` INTEGER, 
  `APNovillas` INTEGER, 
  `APMautas` INTEGER, 
  `APBecerras` INTEGER, 
  `APToros` INTEGER, 
  `APNovillos` INTEGER, 
  `APMautes` INTEGER, 
  `APBecerros` INTEGER, 
  `NBecerras` INTEGER, 
  `NBecerros` INTEGER, 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  INDEX (`IdCierre`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'CierreProduccion'
#

DROP TABLE IF EXISTS `CierreProduccion`;

CREATE TABLE `CierreProduccion` (
  `IdCierre` INTEGER NOT NULL, 
  `EnProd1` INTEGER, 
  `EnProd2` INTEGER, 
  `EnProd3` INTEGER, 
  `EnProd4` INTEGER, 
  `EnProd5` INTEGER, 
  `NPesajeNSecado1` INTEGER, 
  `NPesajeNSecado2` INTEGER, 
  `NPesajeNSecado3` INTEGER, 
  `NPesajeNSecado4` INTEGER, 
  `NPesajeNSecado5` INTEGER, 
  `AcumProd1` FLOAT NULL, 
  `AcumProd2` FLOAT NULL, 
  `AcumProd3` FLOAT NULL, 
  `AcumProd4` FLOAT NULL, 
  `AcumProd5` FLOAT NULL, 
  `NoSecas1` INTEGER, 
  `NoSecas2` INTEGER, 
  `NoSecas3` INTEGER, 
  `NoSecas4` INTEGER, 
  `NoSecas5` INTEGER, 
  `AcumDiasProd1` INTEGER, 
  `AcumDiasProd2` INTEGER, 
  `AcumDiasProd3` INTEGER, 
  `AcumDiasProd4` INTEGER, 
  `AcumDiasProd5` INTEGER, 
  `AcumDiasSeca1` INTEGER, 
  `AcumDiasSeca2` INTEGER, 
  `AcumDiasSeca3` INTEGER, 
  `AcumDiasSeca4` INTEGER, 
  `AcumDiasSeca5` INTEGER, 
  `No2441` INTEGER, 
  `No2442` INTEGER, 
  `No2443` INTEGER, 
  `No2444` INTEGER, 
  `No2445` INTEGER, 
  `Acum2441` FLOAT NULL, 
  `Acum2442` FLOAT NULL, 
  `Acum2443` FLOAT NULL, 
  `Acum2444` FLOAT NULL, 
  `Acum2445` FLOAT NULL, 
  `No3051` INTEGER, 
  `No3052` INTEGER, 
  `No3053` INTEGER, 
  `No3054` INTEGER, 
  `No3055` INTEGER, 
  `Acum3051` FLOAT NULL, 
  `Acum3052` FLOAT NULL, 
  `Acum3053` FLOAT NULL, 
  `Acum3054` FLOAT NULL, 
  `Acum3055` FLOAT NULL, 
  `AParir1` INTEGER, 
  `AParir2` INTEGER, 
  `AParir3` INTEGER, 
  `AParir4` INTEGER, 
  `AParir5` INTEGER, 
  `AParir6` INTEGER, 
  `ASecar1` INTEGER, 
  `ASecar2` INTEGER, 
  `ASecar3` INTEGER, 
  `ASecar4` INTEGER, 
  `ASecar5` INTEGER, 
  `ASecar6` INTEGER, 
  `NovAParir1` INTEGER, 
  `NovAParir2` INTEGER, 
  `NovAParir3` INTEGER, 
  `NovAParir4` INTEGER, 
  `NovAParir5` INTEGER, 
  `NovAParir6` INTEGER, 
  `DestetesM1` INTEGER, 
  `DestetesM2` INTEGER, 
  `DestetesM3` INTEGER, 
  `DestetesM4` INTEGER, 
  `DestetesM5` INTEGER, 
  `DestetesH1` INTEGER, 
  `DestetesH2` INTEGER, 
  `DestetesH3` INTEGER, 
  `DestetesH4` INTEGER, 
  `DestetesH5` INTEGER, 
  `AcumPesoDestetesM1` FLOAT NULL, 
  `AcumPesoDestetesM2` FLOAT NULL, 
  `AcumPesoDestetesM3` FLOAT NULL, 
  `AcumPesoDestetesM4` FLOAT NULL, 
  `AcumPesoDestetesM5` FLOAT NULL, 
  `AcumPesoDestetesH1` FLOAT NULL, 
  `AcumPesoDestetesH2` FLOAT NULL, 
  `AcumPesoDestetesH3` FLOAT NULL, 
  `AcumPesoDestetesH4` FLOAT NULL, 
  `AcumPesoDestetesH5` FLOAT NULL, 
  `AcumDiasDestetesM1` INTEGER, 
  `AcumDiasDestetesM2` INTEGER, 
  `AcumDiasDestetesM3` INTEGER, 
  `AcumDiasDestetesM4` INTEGER, 
  `AcumDiasDestetesM5` INTEGER, 
  `AcumDiasDestetesH1` INTEGER, 
  `AcumDiasDestetesH2` INTEGER, 
  `AcumDiasDestetesH3` INTEGER, 
  `AcumDiasDestetesH4` INTEGER, 
  `AcumDiasDestetesH5` INTEGER
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'CierreReproduccion'
#

DROP TABLE IF EXISTS `CierreReproduccion`;

CREATE TABLE `CierreReproduccion` (
  `IdCierre` INTEGER NOT NULL, 
  `NovillasE` INTEGER, 
  `NovillasS` INTEGER, 
  `NovillasP` INTEGER, 
  `NovillasV` INTEGER, 
  `VacasE` INTEGER, 
  `VacasS` INTEGER, 
  `VacasP` INTEGER, 
  `VacasV` INTEGER, 
  `NovillasI1` INTEGER, 
  `NovillasI2` INTEGER, 
  `NovillasI3` INTEGER, 
  `NovillasI4` INTEGER, 
  `VacasI1` INTEGER, 
  `VacasI2` INTEGER, 
  `VacasI3` INTEGER, 
  `VacasI4` INTEGER, 
  `NovillasM1` INTEGER, 
  `NovillasM2` INTEGER, 
  `NovillasM3` INTEGER, 
  `NovillasM4` INTEGER, 
  `VacasM1` INTEGER, 
  `VacasM2` INTEGER, 
  `VacasM3` INTEGER, 
  `VacasM4` INTEGER, 
  `Novillas1I` INTEGER, 
  `Novillas2I` INTEGER, 
  `Novillas3I` INTEGER, 
  `Novillas4I` INTEGER, 
  `Novillas1M` INTEGER, 
  `Novillas2M` INTEGER, 
  `Novillas3M` INTEGER, 
  `Novillas4M` INTEGER, 
  `Vacas1I` INTEGER, 
  `Vacas2I` INTEGER, 
  `Vacas3I` INTEGER, 
  `Vacas4I` INTEGER, 
  `Vacas1M` INTEGER, 
  `Vacas2M` INTEGER, 
  `Vacas3M` INTEGER, 
  `Vacas4M` INTEGER, 
  `PartosN1` INTEGER, 
  `PartosN2` INTEGER, 
  `PartosN3` INTEGER, 
  `PartosN4` INTEGER, 
  `PartosN5` INTEGER, 
  `PartosA1` INTEGER, 
  `PartosA2` INTEGER, 
  `PartosA3` INTEGER, 
  `PartosA4` INTEGER, 
  `PartosA5` INTEGER, 
  `PartosD1` INTEGER, 
  `PartosD2` INTEGER, 
  `PartosD3` INTEGER, 
  `PartosD4` INTEGER, 
  `PartosD5` INTEGER, 
  `PartosNM1` INTEGER, 
  `PartosNM2` INTEGER, 
  `PartosNM3` INTEGER, 
  `PartosNM4` INTEGER, 
  `PartosNM5` INTEGER, 
  `PartosM1` INTEGER, 
  `PartosM2` INTEGER, 
  `PartosM3` INTEGER, 
  `PartosM4` INTEGER, 
  `PartosM5` INTEGER, 
  `PartosH1` INTEGER, 
  `PartosH2` INTEGER, 
  `PartosH3` INTEGER, 
  `PartosH4` INTEGER, 
  `PartosH5` INTEGER, 
  `PesoM1` FLOAT NULL, 
  `PesoM2` FLOAT NULL, 
  `PesoM3` FLOAT NULL, 
  `PesoM4` FLOAT NULL, 
  `PesoM5` FLOAT NULL, 
  `PesoH1` FLOAT NULL, 
  `PesoH2` FLOAT NULL, 
  `PesoH3` FLOAT NULL, 
  `PesoH4` FLOAT NULL, 
  `PesoH5` FLOAT NULL, 
  `RevisionesP` INTEGER, 
  `RevisionesV` INTEGER, 
  `CelosSS` INTEGER, 
  `Abortos` INTEGER, 
  `Inseminaciones` INTEGER, 
  `Montas` INTEGER, 
  `InseminacionesEspera` INTEGER DEFAULT 0, 
  `MontasEspera` INTEGER DEFAULT 0, 
  `Mir_Calculado` INTEGER DEFAULT 0, 
  `Mir_Parametro` INTEGER DEFAULT 0
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Codigos'
#

DROP TABLE IF EXISTS `Codigos`;

CREATE TABLE `Codigos` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `TipoCodigo` INTEGER NOT NULL, 
  `Codigo` VARCHAR(4) NOT NULL, 
  `DescripcionCodigo` VARCHAR(40), 
  `FechaUltimaPostura` DATETIME, 
  `Intervalo` INTEGER, 
  `CodigoComun` TINYINT(1) NOT NULL, 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  `DG` TINYINT(1) DEFAULT 1, 
  `DC` TINYINT(1) DEFAULT 1, 
  `DU` TINYINT(1) DEFAULT 1, 
  `DD` TINYINT(1) DEFAULT 1, 
  `DI` TINYINT(1) DEFAULT 1, 
  `DV` TINYINT(1) DEFAULT 1
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'ComposicionRacialAnimal'
#

DROP TABLE IF EXISTS `ComposicionRacialAnimal`;

CREATE TABLE `ComposicionRacialAnimal` (
  `IdDatosGeneralesAnimal` INTEGER NOT NULL DEFAULT 0, 
  `IdRaza` INTEGER NOT NULL DEFAULT 0, 
  `Porcentaje` FLOAT NOT NULL DEFAULT 0, 
  `ModificadoPor` VARCHAR(20) NOT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'CondicionesReportes'
#

DROP TABLE IF EXISTS `CondicionesReportes`;

CREATE TABLE `CondicionesReportes` (
  `IdRep` INTEGER NOT NULL, 
  `CodVarRep` VARCHAR(6) NOT NULL, 
  `TipoComparacion` INTEGER NOT NULL, 
  `ATexto` VARCHAR(50), 
  `BTexto` VARCHAR(50), 
  `ANumero` DOUBLE NULL, 
  `BNumero` DOUBLE NULL, 
  `AFecha` DATETIME, 
  `BFecha` DATETIME, 
  `SiNo` TINYINT(1), 
  `ModificadoPor` VARCHAR(20) NOT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Configuracion'
#

DROP TABLE IF EXISTS `Configuracion`;

CREATE TABLE `Configuracion` (
  `Actualizador` TINYINT(1) DEFAULT 1, 
  `Version` VARCHAR(8), 
  `Temporal1` VARCHAR(50), 
  `Temporal2` VARCHAR(50), 
  `FincaActiva` VARCHAR(6), 
  `FechaReporte` DATETIME, 
  `CondCorpCom` TINYINT(1), 
  `VaqueraCom` TINYINT(1), 
  `RazaCom` TINYINT(1), 
  `DiagNoRepCom` TINYINT(1), 
  `TratNoRepCom` TINYINT(1), 
  `ClasificacionCom` TINYINT(1), 
  `PlanSanitCom` TINYINT(1), 
  `ColorCom` TINYINT(1), 
  `DiagRepCom` TINYINT(1), 
  `TratRepCom` TINYINT(1), 
  `NombreFondo` VARCHAR(100), 
  `TituloReporteGenerico` VARCHAR(50), 
  `Encabezado1` LONGTEXT, 
  `Encabezado2` LONGTEXT, 
  `LineaReporteGenerico` LONGTEXT, 
  `Lote` INTEGER, 
  `Vaquera` VARCHAR(50), 
  `TamañoPracticosReportes` INTEGER NOT NULL DEFAULT 12, 
  `TamañoUnicosReportes` INTEGER NOT NULL DEFAULT 12, 
  `TamañoTecnicosReportes` INTEGER NOT NULL DEFAULT 12, 
  `ArchivoEnvioNacimientos` VARCHAR(50), 
  `ArchivoEnvioEstado` VARCHAR(50), 
  `ArchivoEnvioVaqueras` VARCHAR(50), 
  `ArchivoEnvioSemen` VARCHAR(255), 
  `ArchivoRecepcion` VARCHAR(255), 
  `ArchivoConfirmacion` VARCHAR(255), 
  `ArchivoError` VARCHAR(255), 
  `FechaUltimoRespaldo` DATETIME, 
  `PeriodicidadRespaldo` TINYINT(3) UNSIGNED DEFAULT 0, 
  `SugerirBarridos` TINYINT(1), 
  `EntradaVientres` TINYINT(1) DEFAULT 0, 
  `EntradaNoVientres` TINYINT(1) DEFAULT 0, 
  `EntradaToros` TINYINT(1) DEFAULT 0, 
  `EntradaArbol` TINYINT(1) DEFAULT 0, 
  `EntradaTecnicos` TINYINT(1) DEFAULT 0, 
  `EntradaDGA` TINYINT(1) DEFAULT 0
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'ConfiguracionReportes'
#

DROP TABLE IF EXISTS `ConfiguracionReportes`;

CREATE TABLE `ConfiguracionReportes` (
  `IdRep` INTEGER NOT NULL AUTO_INCREMENT, 
  `TipoReporte` VARCHAR(2) NOT NULL, 
  `CodRep` INTEGER NOT NULL, 
  `DescripcionReporte` VARCHAR(50), 
  `SoloImpresora` TINYINT(1) DEFAULT 0, 
  `SaltoLinea` INTEGER DEFAULT 1, 
  `EspacioEntreColumnas` INTEGER DEFAULT 1, 
  `Resumen` TINYINT(1) DEFAULT 0, 
  `Externo` TINYINT(1) DEFAULT 0, 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  `ReporteComun` TINYINT(1) NOT NULL DEFAULT 0, 
  `NombreReporteExterno` VARCHAR(50), 
  `QuienVa` VARCHAR(1) NOT NULL DEFAULT 'T', 
  `CuantosVan` INTEGER, 
  `SQL` LONGTEXT, 
  `TamañoFuente` INTEGER DEFAULT 10, 
  `PapelVertical` TINYINT(1) DEFAULT 1, 
  `TamañoPapel` INTEGER DEFAULT 1, 
  INDEX (`IdRep`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'DatosGeneralesAnimal'
#

DROP TABLE IF EXISTS `DatosGeneralesAnimal`;

CREATE TABLE `DatosGeneralesAnimal` (
  `IDDatosGeneralesAnimal` INTEGER AUTO_INCREMENT, 
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `Practico` VARCHAR(12), 
  `NombreAnimal` VARCHAR(40), 
  `Raza` VARCHAR(4), 
  `Sexo` VARCHAR(10) NOT NULL, 
  `Estado` VARCHAR(10) NOT NULL, 
  `ProcedenciaAnimal` VARCHAR(20), 
  `NumeroRegistro` VARCHAR(12), 
  `FechaRegistro` DATETIME, 
  `AsociacionRegistro` VARCHAR(40), 
  `UnicoPadre` VARCHAR(12), 
  `UnicoMadre` VARCHAR(12), 
  `ComentarioArbol` LONGTEXT, 
  `Color` VARCHAR(4), 
  `Clasificacion` VARCHAR(4), 
  `Tetas` INTEGER, 
  `Foto` VARCHAR(255), 
  `ImagenAnimal` LONGBLOB, 
  `ComentarioActual` VARCHAR(40), 
  `FechaComentarioActual` DATETIME, 
  `OrigenNacimiento` VARCHAR(10) NOT NULL DEFAULT 'Insemin', 
  `Portadora` VARCHAR(12), 
  `NumeroPartoMadre` INTEGER NOT NULL DEFAULT 1, 
  `CruceAnimal` VARCHAR(12), 
  `TipoExplotacion` VARCHAR(10) NOT NULL DEFAULT 'Doble', 
  `Descartado` TINYINT(1) DEFAULT 0, 
  `FechaNacimiento` DATETIME, 
  `Inscrito` TINYINT(1), 
  `Eliminado` TINYINT(1), 
  `ArbolComun` TINYINT(1) NOT NULL DEFAULT 0, 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  `ToroComun` TINYINT(1) DEFAULT 0, 
  `VaEnReporte` TINYINT(1) DEFAULT 0, 
  `IdentificadorElectronico` VARCHAR(20), 
  `ComposicionRacial` VARCHAR(20), 
  `OrigenDatos` TINYINT(1) DEFAULT 0, 
  `Ganadoble` VARCHAR(10), 
  `P244Madre` FLOAT NULL, 
  `P305Madre` FLOAT NULL, 
  `UnicoAbuelaMaterna` VARCHAR(12), 
  `P244AbuelaMaterna` FLOAT NULL, 
  `P305AbuelaMaterna` FLOAT NULL, 
  `Registro` TINYINT(1) DEFAULT 0, 
  `NivelRegistro` TINYINT(3) UNSIGNED, 
  INDEX (`IDDatosGeneralesAnimal`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Grupos_Otras_Variables'
#

DROP TABLE IF EXISTS `Grupos_Otras_Variables`;

CREATE TABLE `Grupos_Otras_Variables` (
  `id_Grupo` INTEGER NOT NULL AUTO_INCREMENT, 
  `Nombre_Grupo` VARCHAR(20) NOT NULL, 
  INDEX (`id_Grupo`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Haciendas'
#

DROP TABLE IF EXISTS `Haciendas`;

CREATE TABLE `Haciendas` (
  `Serial1` VARCHAR(10), 
  `Serial2` VARCHAR(10), 
  `CodHa` VARCHAR(6) NOT NULL, 
  `ClaveHa` VARCHAR(8), 
  `NombreHa` VARCHAR(40) NOT NULL, 
  `Propietario` VARCHAR(40) NOT NULL, 
  `Ubicacion` VARCHAR(40), 
  `AreaTotal` FLOAT NULL, 
  `AreaPasto` FLOAT NULL, 
  `AreaInfra` FLOAT NULL, 
  `AreaOtros` FLOAT NULL, 
  `AreaSinCult` FLOAT NULL, 
  `VacasIn` INTEGER DEFAULT 0, 
  `NovillasIn` INTEGER DEFAULT 0, 
  `MautasIn` INTEGER DEFAULT 0, 
  `BecerrasIn` INTEGER DEFAULT 0, 
  `TorosIn` INTEGER DEFAULT 0, 
  `NovillosIn` INTEGER DEFAULT 0, 
  `MautesIn` INTEGER DEFAULT 0, 
  `BecerrosIn` INTEGER DEFAULT 0, 
  `VacasMan` INTEGER DEFAULT 0, 
  `NovillasMan` INTEGER DEFAULT 0, 
  `MautasMan` INTEGER DEFAULT 0, 
  `BecerrasMan` INTEGER DEFAULT 0, 
  `TorosMan` INTEGER DEFAULT 0, 
  `NovillosMan` INTEGER DEFAULT 0, 
  `MautesMan` INTEGER DEFAULT 0, 
  `BecerrosMan` INTEGER DEFAULT 0, 
  `RevisionPostServicio` INTEGER DEFAULT 45, 
  `RevisionPostParto` INTEGER DEFAULT 30, 
  `DiasSecadoPreparto` INTEGER DEFAULT 60, 
  `LimiteAbortoAB` INTEGER DEFAULT 152, 
  `DiasEntreRevisiones` INTEGER DEFAULT 30, 
  `NumeroOrdeños` INTEGER DEFAULT 2, 
  `DiasProximoParto` INTEGER DEFAULT 285, 
  `DiasProximoCelo` INTEGER DEFAULT 21, 
  `PromEdad1ServicioE` INTEGER, 
  `PromEdad1PartoE` INTEGER, 
  `PromParto1CeloE` INTEGER, 
  `PromParto1ServicioE` INTEGER, 
  `PromDiasGestacionE` INTEGER, 
  `PromPartoConcepE` INTEGER, 
  `PromPartoPartoE` INTEGER, 
  `Prom1ServicioConcepE` INTEGER, 
  `PromDiasEntreCelosE` INTEGER, 
  `PromInsConcepE` FLOAT NULL, 
  `PromMonConcepE` FLOAT NULL, 
  `PromSerConcepE` FLOAT NULL, 
  `PromRevEntrePartosE` FLOAT NULL, 
  `PromDiasEntreRevE` INTEGER, 
  `PromEdad1ServicioC` INTEGER DEFAULT 26, 
  `PromEdad1PartoC` INTEGER DEFAULT 36, 
  `PromParto1CeloC` INTEGER DEFAULT 35, 
  `PromParto1ServicioC` INTEGER DEFAULT 60, 
  `PromDiasGestacionC` INTEGER DEFAULT 285, 
  `PromPartoConcepC` INTEGER DEFAULT 110, 
  `PromPartoPartoC` INTEGER DEFAULT 400, 
  `Prom1ServicioConcepC` INTEGER DEFAULT 50, 
  `PromDiasEntreCelosC` INTEGER DEFAULT 21, 
  `PromInsConcepC` FLOAT NULL DEFAULT 2, 
  `PromMonConcepC` FLOAT NULL DEFAULT 2, 
  `PromSerConcepC` FLOAT NULL DEFAULT 2, 
  `PromRevEntrePartosC` FLOAT NULL DEFAULT 1, 
  `PromDiasEntreRevC` INTEGER DEFAULT 60, 
  `PromDiasLactanciaE` INTEGER, 
  `PromDiasSecaE` INTEGER, 
  `Prom244E` FLOAT NULL, 
  `Prom305E` FLOAT NULL, 
  `PromProdTotalE` FLOAT NULL, 
  `PromProdEntrePartosE` FLOAT NULL, 
  `PromDiasLactanciaC` INTEGER DEFAULT 305, 
  `PromDiasSecaC` INTEGER DEFAULT 60, 
  `Prom244C` FLOAT NULL DEFAULT 1450, 
  `Prom305C` FLOAT NULL DEFAULT 1800, 
  `PromProdTotalC` FLOAT NULL DEFAULT 1800, 
  `PromProdEntrePartosC` FLOAT NULL DEFAULT 5, 
  `VaqueraCom` TINYINT(1) NOT NULL DEFAULT 0, 
  `RazaCom` TINYINT(1) NOT NULL DEFAULT 1, 
  `ColorCom` TINYINT(1) NOT NULL DEFAULT 1, 
  `DiagRepCom` TINYINT(1) NOT NULL DEFAULT 1, 
  `TratRepCom` TINYINT(1) NOT NULL DEFAULT 1, 
  `ClasificacionCom` TINYINT(1) NOT NULL DEFAULT 1, 
  `TecnicoCom` TINYINT(1) NOT NULL DEFAULT 0, 
  `ToroCom` TINYINT(1) NOT NULL DEFAULT 0, 
  `CondCorpCom` TINYINT(1) NOT NULL DEFAULT 1, 
  `ArbolCom` TINYINT(1) NOT NULL DEFAULT 0, 
  `PlanSanitCom` TINYINT(1) NOT NULL DEFAULT 0, 
  `PracticoIgualUnico` TINYINT(1) NOT NULL DEFAULT 0, 
  `InscripcionBecerro` TINYINT(1) NOT NULL DEFAULT 1, 
  `InscripcionBecerra` TINYINT(1) NOT NULL DEFAULT 1, 
  `SecadoDesteteAuto` TINYINT(1) NOT NULL DEFAULT 1, 
  `CambioEstadoPesoAuto` TINYINT(1) NOT NULL DEFAULT 1, 
  `DiagPreñada` VARCHAR(4) DEFAULT '   P', 
  `DiagEspera` VARCHAR(4) DEFAULT '   E', 
  `UsoFactorPesaje` TINYINT(1) NOT NULL DEFAULT 0, 
  `Peso205ME` FLOAT NULL, 
  `Peso365ME` FLOAT NULL, 
  `Peso18ME` FLOAT NULL, 
  `Peso24ME` FLOAT NULL, 
  `PesoNacME` FLOAT NULL, 
  `PesoDesME` FLOAT NULL, 
  `Peso205HE` FLOAT NULL, 
  `Peso365HE` FLOAT NULL, 
  `Peso18HE` FLOAT NULL, 
  `Peso24HE` FLOAT NULL, 
  `PesoNacHE` FLOAT NULL, 
  `PesoDesHE` FLOAT NULL, 
  `Peso205MC` FLOAT NULL DEFAULT 120, 
  `Peso365MC` FLOAT NULL DEFAULT 180, 
  `Peso18MC` FLOAT NULL DEFAULT 250, 
  `Peso24MC` FLOAT NULL DEFAULT 350, 
  `PesoNacMC` FLOAT NULL DEFAULT 30, 
  `PesoDesMC` FLOAT NULL DEFAULT 160, 
  `Peso205HC` FLOAT NULL DEFAULT 120, 
  `Peso365HC` FLOAT NULL DEFAULT 180, 
  `Peso18HC` FLOAT NULL DEFAULT 250, 
  `Peso24HC` FLOAT NULL DEFAULT 350, 
  `PesoNacHC` FLOAT NULL DEFAULT 30, 
  `PesoDesHC` FLOAT NULL DEFAULT 160, 
  `Altura205ME` FLOAT NULL, 
  `Altura365ME` FLOAT NULL, 
  `Altura18ME` FLOAT NULL, 
  `Altura24ME` FLOAT NULL, 
  `AlturaNacME` FLOAT NULL, 
  `AlturaDesME` FLOAT NULL, 
  `Altura205HE` FLOAT NULL, 
  `Altura365HE` FLOAT NULL, 
  `Altura18HE` FLOAT NULL, 
  `Altura24HE` FLOAT NULL, 
  `AlturaNacHE` FLOAT NULL, 
  `AlturaDesHE` FLOAT NULL, 
  `Altura205MC` FLOAT NULL DEFAULT 75, 
  `Altura365MC` FLOAT NULL DEFAULT 100, 
  `Altura18MC` FLOAT NULL DEFAULT 130, 
  `Altura24MC` FLOAT NULL DEFAULT 150, 
  `AlturaNacMC` FLOAT NULL DEFAULT 50, 
  `AlturaDesMC` FLOAT NULL DEFAULT 90, 
  `Altura205HC` FLOAT NULL DEFAULT 75, 
  `Altura365HC` FLOAT NULL DEFAULT 100, 
  `Altura18HC` FLOAT NULL DEFAULT 130, 
  `Altura24HC` FLOAT NULL DEFAULT 150, 
  `AlturaNacHC` FLOAT NULL DEFAULT 50, 
  `AlturaDesHC` FLOAT NULL DEFAULT 90, 
  `DiagNoRepCom` TINYINT(1) NOT NULL DEFAULT 1, 
  `TratNoRepCom` TINYINT(1) NOT NULL DEFAULT 1, 
  `MinimaGestacion` INTEGER DEFAULT 240, 
  `MaximaGestacion` INTEGER DEFAULT 300, 
  `PesoMauta` FLOAT NULL DEFAULT 180, 
  `PesoMaute` FLOAT NULL DEFAULT 180, 
  `PesoNovillo` FLOAT NULL DEFAULT 350, 
  `InventarioAuto` TINYINT(1) NOT NULL DEFAULT 1, 
  `FechaEstadisticaProduccion` DATETIME, 
  `FechaEstadisticaReproduccion` DATETIME, 
  `FechaEstadisticaCrecimiento` DATETIME, 
  `MaxProdVacaAvio` INTEGER DEFAULT 30, 
  `TecnicoPredeterminado` VARCHAR(12), 
  `ProxCriaM` VARCHAR(12), 
  `ProxCriaH` VARCHAR(12), 
  `VaqVacaParto` VARCHAR(4), 
  `VaqVacaSecado` VARCHAR(4), 
  `VaqVacaPreñada` VARCHAR(4), 
  `VaqNovillaParto` VARCHAR(4), 
  `VaqNovillaPreñada` VARCHAR(4), 
  `VaqCriaMParto` VARCHAR(4), 
  `VaqCriaHParto` VARCHAR(4), 
  `VaqCriaMDestete` VARCHAR(4), 
  `VaqCriaHDestete` VARCHAR(4), 
  `NumeroCorridoMachos` TINYINT(1) DEFAULT 0, 
  `NumeroCorridoHembras` TINYINT(1) DEFAULT 0, 
  `ToroIndeterminado` VARCHAR(12) DEFAULT '  INDEFINIDO', 
  `TecnicoIndeterminado` VARCHAR(12) DEFAULT ' DESCONOCIDO', 
  `PromGanEntrePartosE` INTEGER, 
  `PromGanEntrePartosC` INTEGER DEFAULT 450, 
  `CambiarBecerroConMadre` TINYINT(1) DEFAULT 1, 
  `SAP` TINYINT(1) DEFAULT 0, 
  `ChequearPorCelos` TINYINT(1) NOT NULL DEFAULT 1, 
  `DiasCelos` INTEGER NOT NULL DEFAULT 7, 
  `ChequearPorCitas` TINYINT(1) NOT NULL DEFAULT 1, 
  `DiasCitas` INTEGER NOT NULL DEFAULT 1, 
  `ChequearPorPlanes` TINYINT(1) NOT NULL DEFAULT 1, 
  `DiasPlanes` INTEGER NOT NULL DEFAULT 30, 
  `ChequearPorPartos` TINYINT(1) NOT NULL DEFAULT 1, 
  `DiasPartos` INTEGER NOT NULL DEFAULT 15, 
  `ChequearPorSecados` TINYINT(1) NOT NULL DEFAULT 1, 
  `DiasSecados` INTEGER NOT NULL DEFAULT 15, 
  `MesesCrias` INTEGER NOT NULL DEFAULT 30, 
  `AsociacionRegistro` VARCHAR(40), 
  `CodigoGanadoble` VARCHAR(3), 
  `DiasInactividad` INTEGER NOT NULL DEFAULT 60, 
  `ChequearDiasInactividad` TINYINT(1) NOT NULL DEFAULT 1
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Haciendas_Configuracion'
#

DROP TABLE IF EXISTS `Haciendas_Configuracion`;

CREATE TABLE `Haciendas_Configuracion` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Dias_Becerros` INTEGER DEFAULT 60, 
  `Dias_Becerras` INTEGER DEFAULT 60, 
  `Dias_Mautes` INTEGER DEFAULT 60, 
  `Dias_Mautas` INTEGER DEFAULT 60, 
  `Dias_Novillos` INTEGER DEFAULT 60, 
  `Dias_Novillas` INTEGER DEFAULT 60, 
  `Dias_Vacas` INTEGER DEFAULT 60, 
  `Dias_Toros` INTEGER DEFAULT 60, 
  `Dias_Total` INTEGER NOT NULL DEFAULT 0, 
  `con_Produccion_Filtrar` TINYINT(1), 
  `con_EAR_Filtrar` TINYINT(1), 
  `con_CC_Filtrar` TINYINT(1), 
  `con_DP_Filtrar` TINYINT(1), 
  `con_Produccion_Formula` VARCHAR(100) DEFAULT 'rel(8:12:3:1);ran(12:20:4);ran(20:100:6)', 
  `con_EAR_Formula` VARCHAR(100) DEFAULT 'opc(p:0.5);opc(v:1);opc(e:1)', 
  `con_CC_Formula` VARCHAR(100) DEFAULT 'ran(1:3:1);ran(3:5:0.25)', 
  `con_DP_Formula` VARCHAR(100) DEFAULT 'ran(0:90:0.5);ran(90:120:1);ran(120:240:0.25)', 
  `IDEspecie` INTEGER NOT NULL DEFAULT 1, 
  `Tipo_Explotacion` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1, 
  `AHCPA` VARCHAR(12) NOT NULL DEFAULT 'Vacas', 
  `AHSPA` VARCHAR(12) NOT NULL DEFAULT 'Novillas', 
  `DHSPA` VARCHAR(12) NOT NULL DEFAULT 'Mautas', 
  `LHSPA` VARCHAR(12) NOT NULL DEFAULT 'Becerras', 
  `AMREP` VARCHAR(12) NOT NULL DEFAULT 'Toros', 
  `AMNRP` VARCHAR(12) NOT NULL DEFAULT 'Novillos', 
  `DMNRP` VARCHAR(12) NOT NULL DEFAULT 'Mautes', 
  `LMNRP` VARCHAR(12) NOT NULL DEFAULT 'Becerros', 
  `Chequear_Dias_Inactividad_AHCPA` TINYINT(1) NOT NULL DEFAULT 1, 
  `Dias_Inactividad_AHCPA` INTEGER NOT NULL DEFAULT 60, 
  `Chequear_Dias_Inactividad_AHSPA` TINYINT(1) NOT NULL DEFAULT 1, 
  `Dias_Inactividad_AHSPA` INTEGER NOT NULL DEFAULT 60, 
  `Chequear_Dias_Inactividad_DHSPA` TINYINT(1) NOT NULL DEFAULT 1, 
  `Dias_Inactividad_DHSPA` INTEGER NOT NULL DEFAULT 60, 
  `Chequear_Dias_Inactividad_LHSPA` TINYINT(1) NOT NULL DEFAULT 1, 
  `Dias_Inactividad_LHSPA` INTEGER NOT NULL DEFAULT 60, 
  `Chequear_Dias_Inactividad_AMREP` TINYINT(1) NOT NULL DEFAULT 1, 
  `Dias_Inactividad_AMREP` INTEGER NOT NULL DEFAULT 60, 
  `Chequear_Dias_Inactividad_AMNRP` TINYINT(1) NOT NULL DEFAULT 1, 
  `Dias_Inactividad_AMNRP` INTEGER NOT NULL DEFAULT 60, 
  `Chequear_Dias_Inactividad_DMNRP` TINYINT(1) NOT NULL DEFAULT 1, 
  `Dias_Inactividad_DMNRP` INTEGER NOT NULL DEFAULT 60, 
  `Chequear_Dias_Inactividad_LMNRP` TINYINT(1) NOT NULL DEFAULT 1, 
  `Dias_Inactividad_LMNRP` INTEGER NOT NULL DEFAULT 60, 
  `Vaquera` VARCHAR(10) NOT NULL DEFAULT 'Vaqueras', 
  `Habilitar_Pesajes_Leche` TINYINT(1) DEFAULT 1, 
  `Habilitar_Pesos_Corporales` TINYINT(1) DEFAULT 1, 
  `Habilitar_Plan_Sanitario` TINYINT(1) DEFAULT 1, 
  `Habilitar_No_Reproductivos` TINYINT(1) DEFAULT 1, 
  `Habilitar_Composicion_Racial` TINYINT(1) DEFAULT 1, 
  `Habilitar_Hierros` TINYINT(1) DEFAULT 1, 
  `Habilitar_Gansoft_Movil` TINYINT(1) DEFAULT 1, 
  `Habilitar_Otras_Variables` TINYINT(1) DEFAULT 1, 
  `Mir` INTEGER DEFAULT 150
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Hierro'
#

DROP TABLE IF EXISTS `Hierro`;

CREATE TABLE `Hierro` (
  `IdHierro` INTEGER AUTO_INCREMENT, 
  `Estado` VARCHAR(2) NOT NULL, 
  `Ano` VARCHAR(4) NOT NULL, 
  `NumeroRegistro` VARCHAR(6) NOT NULL, 
  `Propietario` VARCHAR(40) NOT NULL, 
  `Uso` VARCHAR(20), 
  `NombreImagen` VARCHAR(255), 
  `ImagenHierro` LONGBLOB, 
  INDEX (`IdHierro`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'HierroAnimal'
#

DROP TABLE IF EXISTS `HierroAnimal`;

CREATE TABLE `HierroAnimal` (
  `IdHierro` INTEGER DEFAULT 0, 
  `IDDatosGeneralesAnimal` INTEGER NOT NULL DEFAULT 0, 
  `Criador` TINYINT(1) DEFAULT 0, 
  `Matador` TINYINT(1) DEFAULT 0
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'HistoriaClinica'
#

DROP TABLE IF EXISTS `HistoriaClinica`;

CREATE TABLE `HistoriaClinica` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaNoRep` DATETIME NOT NULL, 
  `DiagNoRep` VARCHAR(4), 
  `TratNoRep` VARCHAR(4), 
  `ComentarioClinico` VARCHAR(40), 
  `Eficiencia` INTEGER, 
  `ModificadoPor` VARCHAR(20) NOT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Historico_Otras_Variables'
#

DROP TABLE IF EXISTS `Historico_Otras_Variables`;

CREATE TABLE `Historico_Otras_Variables` (
  `id_Historico_Otras_Variables` INTEGER NOT NULL AUTO_INCREMENT, 
  `id_Variable` INTEGER NOT NULL DEFAULT 0, 
  `IDDatosGeneralesAnimal` INTEGER NOT NULL DEFAULT 0, 
  `Fecha_Historico_Otras_Variables` DATETIME NOT NULL, 
  `Valor_Variable` VARCHAR(40) NOT NULL, 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  INDEX (`id_Historico_Otras_Variables`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'HistoricoCambios'
#

DROP TABLE IF EXISTS `HistoricoCambios`;

CREATE TABLE `HistoricoCambios` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `TipoCambio` INTEGER NOT NULL, 
  `FechaCambio` DATETIME NOT NULL, 
  `CodigoVoCC` VARCHAR(4), 
  `ComentarioCambio` VARCHAR(40), 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  `EstadoMovimientoSAP` VARCHAR(1) DEFAULT '0', 
  `ID` INTEGER AUTO_INCREMENT, 
  INDEX (`ID`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'HistoricoComentarios'
#

DROP TABLE IF EXISTS `HistoricoComentarios`;

CREATE TABLE `HistoricoComentarios` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaComentario` DATETIME NOT NULL, 
  `DescripcionComentario` VARCHAR(40), 
  `ModificadoPor` VARCHAR(20) NOT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'HistoricoLactancias'
#

DROP TABLE IF EXISTS `HistoricoLactancias`;

CREATE TABLE `HistoricoLactancias` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaComienzo` DATETIME NOT NULL, 
  `NumeroLactancia` INTEGER NOT NULL, 
  `DiasProd` INTEGER, 
  `ProdTotal` FLOAT NULL, 
  `Prod244` FLOAT NULL, 
  `Prod305` FLOAT NULL, 
  `FechaSecado` DATETIME, 
  `FechaDestete` DATETIME, 
  `PesoDestete1` FLOAT NULL, 
  `PesoDestete2` FLOAT NULL, 
  `ComentarioLactancia` VARCHAR(40), 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  `ProduccionEntrePartos` FLOAT NULL, 
  `DiasSeca` INTEGER, 
  `GananciaEntrePartos` FLOAT NULL, 
  `Total_Destetados_Machos` INTEGER, 
  `Total_Destetados_Hembras` INTEGER, 
  `Peso_Destetados_Machos` FLOAT NULL, 
  `Peso_Destetados_Hembras` FLOAT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'HistoricoPlanSanitario'
#

DROP TABLE IF EXISTS `HistoricoPlanSanitario`;

CREATE TABLE `HistoricoPlanSanitario` (
  `IDHistoricoPlan` INTEGER AUTO_INCREMENT, 
  `CodHa` VARCHAR(6) NOT NULL, 
  `Codigo` VARCHAR(4) NOT NULL, 
  `FechaUltimaPostura` DATETIME, 
  `NumeroDosis` INTEGER, 
  `CodigoComun` TINYINT(1) NOT NULL, 
  `Observaciones` VARCHAR(40), 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  INDEX (`IDHistoricoPlan`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'HistoricoReproduccion'
#

DROP TABLE IF EXISTS `HistoricoReproduccion`;

CREATE TABLE `HistoricoReproduccion` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaRep` DATETIME NOT NULL, 
  `TipoRep` VARCHAR(10) NOT NULL, 
  `NoRep` INTEGER NOT NULL, 
  `PesoCria1Dosis` FLOAT NULL, 
  `TSEUnicoCria1` VARCHAR(12), 
  `TecnicoUnicoCria2` VARCHAR(12), 
  `SexoCria1HoraServicio` VARCHAR(10), 
  `TipoPartoAborto` VARCHAR(10), 
  `DiagGeneral` VARCHAR(4), 
  `DiagCervix` VARCHAR(4), 
  `DiagUtero` VARCHAR(4), 
  `DiagOvarDer` VARCHAR(4), 
  `DiagOvarIzq` VARCHAR(4), 
  `DiagVagina` VARCHAR(4), 
  `TratRep` VARCHAR(4), 
  `EficienciaPesoCria2` FLOAT NULL, 
  `SexoCria2` VARCHAR(10), 
  `Desactivado` TINYINT(1), 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  `MurioCria1` TINYINT(1), 
  `MurioCria2` TINYINT(1), 
  `EstadoMovimientoSAP` VARCHAR(1) DEFAULT '0', 
  `ID` INTEGER AUTO_INCREMENT, 
  `Parto1Celo` INTEGER, 
  `Parto1Servicio` INTEGER, 
  `DiasGestacion` INTEGER, 
  `PartoConcepcion` INTEGER, 
  `PartoParto` INTEGER, 
  `PrimerServicioConcep` INTEGER, 
  `DiasEntreCelos` INTEGER, 
  `InsConcep` INTEGER, 
  `MonConcep` INTEGER, 
  `ServConcep` INTEGER, 
  `RevEntrePartos` INTEGER, 
  `DiasEntreRev` INTEGER, 
  `Total_Machos_Nacidos` INTEGER DEFAULT 0, 
  `Total_Hembras_Nacidas` INTEGER DEFAULT 0, 
  `Total_Machos_Vivos` INTEGER DEFAULT 0, 
  `Total_Hembras_Vivas` INTEGER DEFAULT 0, 
  INDEX (`ID`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Movil'
#

DROP TABLE IF EXISTS `Movil`;

CREATE TABLE `Movil` (
  `Activo` TINYINT(1) DEFAULT 0, 
  `FechaContrato` DATETIME, 
  `TieneContrato` TINYINT(1) DEFAULT 0, 
  `AvisoVencimientoPoliza` TINYINT(1) DEFAULT 0, 
  `FechaUltimaActualizacionMDB` DATETIME, 
  `Historico_Reproduccion_Dias` INTEGER DEFAULT 0, 
  `Historico_Lactancias_Dias` INTEGER DEFAULT 0, 
  `Historicos_Cambios_Vaq_Dias` INTEGER DEFAULT 0, 
  `Historicos_Cambios_Cond_Corp_Dias` INTEGER DEFAULT 0, 
  `Pesajes_Ultima_Lactancia_Dias` INTEGER DEFAULT 0, 
  `Pesos_Corporales_Dias` INTEGER DEFAULT 0, 
  `Historia_Clinica_Dias` INTEGER DEFAULT 0, 
  `Historico_Comentarios_Dias` INTEGER DEFAULT 0, 
  `Historico_Inventarios_Dias` INTEGER DEFAULT 0
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'MovimientosRebaño'
#

DROP TABLE IF EXISTS `MovimientosRebaño`;

CREATE TABLE `MovimientosRebaño` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12), 
  `Estado` VARCHAR(10), 
  `TipoMovimiento` INTEGER NOT NULL, 
  `FechaMovimiento` DATETIME NOT NULL, 
  `Comentario` VARCHAR(40), 
  `NumeroAnimales` INTEGER, 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  `Eliminado` TINYINT(1), 
  `EstadoMovimientoSAP` VARCHAR(1) DEFAULT '0', 
  `ID` INTEGER AUTO_INCREMENT, 
  INDEX (`ID`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'OrdenamientoReportes'
#

DROP TABLE IF EXISTS `OrdenamientoReportes`;

CREATE TABLE `OrdenamientoReportes` (
  `IdRep` INTEGER NOT NULL, 
  `CodVarRep` VARCHAR(6) NOT NULL, 
  `Ascendente` TINYINT(1) NOT NULL DEFAULT 1, 
  `Posicion` INTEGER NOT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Otras_Variables'
#

DROP TABLE IF EXISTS `Otras_Variables`;

CREATE TABLE `Otras_Variables` (
  `id_Variable` INTEGER NOT NULL AUTO_INCREMENT, 
  `id_Grupo` INTEGER DEFAULT 0, 
  `Nombre_Variable` VARCHAR(40) NOT NULL, 
  `Tipo_Variable_Dato` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0, 
  `Posicion_Variable` INTEGER DEFAULT 0, 
  `Historico_Variable` TINYINT(1) DEFAULT 0, 
  `Tipo_Becerra` TINYINT(1) NOT NULL DEFAULT 1, 
  `Tipo_Becerro` TINYINT(1) NOT NULL DEFAULT 1, 
  `Tipo_Mauta` TINYINT(1) NOT NULL DEFAULT 1, 
  `Tipo_Maute` TINYINT(1) NOT NULL DEFAULT 1, 
  `Tipo_Novilla` TINYINT(1) NOT NULL DEFAULT 1, 
  `Tipo_Novillo` TINYINT(1) NOT NULL DEFAULT 1, 
  `Tipo_Vaca` TINYINT(1) NOT NULL DEFAULT 1, 
  `Tipo_Toro` TINYINT(1) NOT NULL DEFAULT 1, 
  `Tipo_Semen` TINYINT(1) NOT NULL DEFAULT 1, 
  `Tipo_Embrión` TINYINT(1) NOT NULL DEFAULT 1, 
  INDEX (`id_Variable`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'PesajesDeLeche'
#

DROP TABLE IF EXISTS `PesajesDeLeche`;

CREATE TABLE `PesajesDeLeche` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaPesaje` DATETIME NOT NULL, 
  `TipoPesaje` INTEGER NOT NULL, 
  `Peso1` FLOAT NULL DEFAULT 0, 
  `Peso2` FLOAT NULL DEFAULT 0, 
  `Peso3` FLOAT NULL DEFAULT 0, 
  `ModificadoPor` VARCHAR(20) NOT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'PesosCorporales'
#

DROP TABLE IF EXISTS `PesosCorporales`;

CREATE TABLE `PesosCorporales` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaPeso` DATETIME NOT NULL, 
  `TipoPeso` VARCHAR(10) NOT NULL, 
  `Peso` FLOAT NULL, 
  `GananciaPeso` INTEGER, 
  `Altura` FLOAT NULL, 
  `GananciaAltura` FLOAT NULL, 
  `DiasEntreFechas` INTEGER, 
  `ModificadoPor` VARCHAR(20) NOT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Raza'
#

DROP TABLE IF EXISTS `Raza`;

CREATE TABLE `Raza` (
  `IdRaza` INTEGER AUTO_INCREMENT, 
  `IDEspecie` INTEGER NOT NULL DEFAULT 1, 
  `CodRaza` VARCHAR(2) NOT NULL, 
  `DescripcionRaza` VARCHAR(20) NOT NULL, 
  `GrupoRaza` VARCHAR(20) NOT NULL, 
  `LecheCarne` VARCHAR(5) NOT NULL, 
  INDEX (`IdRaza`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'ResumenHistoriaClinica'
#

DROP TABLE IF EXISTS `ResumenHistoriaClinica`;

CREATE TABLE `ResumenHistoriaClinica` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `UltDiagNoRep` VARCHAR(4), 
  `UltTratNoRep` VARCHAR(4), 
  `FechaUltNoRep` DATETIME, 
  `PenDiagNoRep` VARCHAR(4), 
  `PenTratNoRep` VARCHAR(4), 
  `FechaPenNoRep` DATETIME, 
  `AntDiagNoRep` VARCHAR(4), 
  `AntTratNoRep` VARCHAR(4), 
  `FechaAntNoRep` DATETIME
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'ResumenHistoricoCambios'
#

DROP TABLE IF EXISTS `ResumenHistoricoCambios`;

CREATE TABLE `ResumenHistoricoCambios` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaUltCambioV` DATETIME, 
  `CodigoUltCambioV` VARCHAR(4), 
  `ComenUltCambioV` VARCHAR(40), 
  `FechaPenCambioV` DATETIME, 
  `CodigoPenCambioV` VARCHAR(4), 
  `FechaAntCambioV` DATETIME, 
  `CodigoAntCambioV` VARCHAR(4), 
  `FechaUltCambioCC` DATETIME, 
  `CodigoUltCambioCC` VARCHAR(4), 
  `ComenUltCambioCC` VARCHAR(40), 
  `FechaPenCambioCC` DATETIME, 
  `CodigoPenCambioCC` VARCHAR(4), 
  `FechaAntCambioCC` DATETIME, 
  `CodigoAntCambioCC` VARCHAR(4), 
  `FechaUltInventario` DATETIME, 
  `CodigoUltInventarioV` VARCHAR(4)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'ResumenHistoricoLactancias'
#

DROP TABLE IF EXISTS `ResumenHistoricoLactancias`;

CREATE TABLE `ResumenHistoricoLactancias` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `DiasProd1` INTEGER, 
  `ProdTotal1` FLOAT NULL, 
  `Prod2441` FLOAT NULL, 
  `Prod3051` FLOAT NULL, 
  `DiasDestete1` INTEGER, 
  `PesoDestete1` FLOAT NULL, 
  `DiasProd2` INTEGER, 
  `ProdTotal2` FLOAT NULL, 
  `Prod2442` FLOAT NULL, 
  `Prod3052` FLOAT NULL, 
  `DiasDestete2` INTEGER, 
  `PesoDestete2` FLOAT NULL, 
  `DiasProd3` INTEGER, 
  `ProdTotal3` FLOAT NULL, 
  `Prod2443` FLOAT NULL, 
  `Prod3053` FLOAT NULL, 
  `DiasDestete3` INTEGER, 
  `PesoDestete3` FLOAT NULL, 
  `DiasProd4` INTEGER, 
  `ProdTotal4` FLOAT NULL, 
  `Prod2444` FLOAT NULL, 
  `Prod3054` FLOAT NULL, 
  `DiasDestete4` INTEGER, 
  `PesoDestete4` FLOAT NULL, 
  `DiasProdP` INTEGER, 
  `ProdTotalP` FLOAT NULL, 
  `Prod244P` FLOAT NULL, 
  `Prod305P` FLOAT NULL, 
  `DiasDesteteP` INTEGER, 
  `PesoDesteteP` FLOAT NULL, 
  `PromDiasProd` FLOAT NULL, 
  `PromProdTotal` FLOAT NULL, 
  `PromProd244` FLOAT NULL, 
  `PromProd305` FLOAT NULL, 
  `PromDiasSeca` FLOAT NULL, 
  `PromProdParto` FLOAT NULL, 
  `PromGanParto` INTEGER, 
  `PromDiasDestete` FLOAT NULL, 
  `PromPesoDestete` FLOAT NULL, 
  `EdadLactancia1` INTEGER, 
  `Total_Destetados_Machos` INTEGER, 
  `Total_Destetados_Hembras` INTEGER, 
  `Peso_Destetados_Machos` FLOAT NULL, 
  `Peso_Destetados_Hembras` FLOAT NULL, 
  `Promedio_Machos_Destetados` FLOAT NULL, 
  `Promedio_Hembras_Destetados` FLOAT NULL, 
  `Peso_Promedio_Machos_Destetados` FLOAT NULL, 
  `Peso_Promedio_Hembras_Destetados` FLOAT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'ResumenHistoricoReproduccion'
#

DROP TABLE IF EXISTS `ResumenHistoricoReproduccion`;

CREATE TABLE `ResumenHistoricoReproduccion` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FecUltParto` DATETIME, 
  `FecUltAborto` DATETIME, 
  `FecUltPartoAborto` DATETIME, 
  `FecUltServicio` DATETIME, 
  `FecUltCelo` DATETIME, 
  `FecUltExamen` DATETIME, 
  `FecPenExamen` DATETIME, 
  `FecProximoExamen` DATETIME, 
  `FecComienzoLactancia` DATETIME, 
  `NoPartos` INTEGER, 
  `NoAbortos` INTEGER, 
  `NoLactancias` INTEGER, 
  `NoRevisiones` INTEGER, 
  `NoMontas` INTEGER, 
  `NoInseminaciones` INTEGER, 
  `NoTransplantes` INTEGER, 
  `NoTotalServicios` INTEGER, 
  `NoCelosSS` INTEGER, 
  `SexoCria1` VARCHAR(10), 
  `SexoCria2` VARCHAR(10), 
  `UnicoCria1` VARCHAR(12), 
  `UnicoCria2` VARCHAR(12), 
  `HoraServicio` VARCHAR(10), 
  `Dosis` FLOAT NULL, 
  `ToroSemenEmbrion` VARCHAR(12), 
  `PadreCriaActual` VARCHAR(12), 
  `Tecnico` VARCHAR(12), 
  `UltDiag` VARCHAR(4), 
  `UltTrat` VARCHAR(4), 
  `UltCervix` VARCHAR(4), 
  `UltUtero` VARCHAR(4), 
  `UltOD` VARCHAR(4), 
  `UltOI` VARCHAR(4), 
  `UltVagina` VARCHAR(4), 
  `PenDiag` VARCHAR(4), 
  `PenTrat` VARCHAR(4), 
  `PenCervix` VARCHAR(4), 
  `PenUtero` VARCHAR(4), 
  `PenOD` VARCHAR(4), 
  `PenOI` VARCHAR(4), 
  `PenVagina` VARCHAR(4), 
  `TipoServicio` VARCHAR(10), 
  `TipoPartoAborto` VARCHAR(10), 
  `EstadoReproductivo` VARCHAR(10), 
  `EdadPrimerServicio` INTEGER, 
  `EdadPrimerParto` INTEGER, 
  `PromParto1Celo` INTEGER, 
  `PromParto1Servicio` INTEGER, 
  `PromDiasGestacion` INTEGER, 
  `PromPartoConcepcion` INTEGER, 
  `PromPartoParto` INTEGER, 
  `Prom1ServicioConcepcion` INTEGER, 
  `PromEntreCelos` INTEGER, 
  `PromDiasEntreCitas` INTEGER, 
  `PromInsXConcepcion` FLOAT NULL, 
  `PromMonXConcepcion` FLOAT NULL, 
  `PromSerXConcepcion` FLOAT NULL, 
  `PromCitasXParto` FLOAT NULL, 
  `PartoParto12` INTEGER, 
  `PartoParto23` INTEGER, 
  `PartoParto34` INTEGER, 
  `PartoParto45` INTEGER, 
  `PartoPartoPenUlt` INTEGER, 
  `DiasVacia12` INTEGER, 
  `DiasVacia23` INTEGER, 
  `DiasVacia34` INTEGER, 
  `DiasVacia45` INTEGER, 
  `DiasVaciaPenUlt` INTEGER, 
  `FechaPrimerServicio` DATETIME, 
  `FechaPrimerParto` DATETIME, 
  `MurioCria1` TINYINT(1), 
  `MurioCria2` TINYINT(1), 
  `FechaPenultimoServicio` DATETIME, 
  `Total_Machos_Nacidos` INTEGER DEFAULT 0, 
  `Total_Hembras_Nacidas` INTEGER DEFAULT 0, 
  `Total_Machos_Vivos` INTEGER DEFAULT 0, 
  `Total_Hembras_Vivas` INTEGER DEFAULT 0, 
  `Promedio_Machos` FLOAT NULL DEFAULT 0, 
  `Promedio_Hembras` FLOAT NULL DEFAULT 0, 
  `Promedio_Machos_Vivos` FLOAT NULL DEFAULT 0, 
  `Promedio_Hembras_Vivas` FLOAT NULL DEFAULT 0
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'ResumenPesajesDeLeche'
#

DROP TABLE IF EXISTS `ResumenPesajesDeLeche`;

CREATE TABLE `ResumenPesajesDeLeche` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaComienzo` DATETIME, 
  `FechaUltPesaje` DATETIME, 
  `FechaPenPesaje` DATETIME, 
  `FechaAntPesaje` DATETIME, 
  `FechaSecado` DATETIME, 
  `PesajeUlt` FLOAT NULL, 
  `PesajePen` FLOAT NULL, 
  `PesajeAnt` FLOAT NULL, 
  `DiasProdAct` INTEGER, 
  `ProdTotalAct` FLOAT NULL, 
  `Prod244Act` FLOAT NULL, 
  `Prod305Act` FLOAT NULL, 
  `ComentarioUltimaLactancia` VARCHAR(40), 
  `PesoDestete1` INTEGER, 
  `PesoDestete2` INTEGER, 
  `Total_Destetados_Machos` INTEGER, 
  `Total_Destetados_Hembras` INTEGER, 
  `Peso_Destetados_Machos` FLOAT NULL, 
  `Peso_Destetados_Hembras` FLOAT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'ResumenPesosCorporales'
#

DROP TABLE IF EXISTS `ResumenPesosCorporales`;

CREATE TABLE `ResumenPesosCorporales` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaNacimiento` DATETIME, 
  `FechaDestete` DATETIME, 
  `FechaIngreso` DATETIME, 
  `FechaUltPeso` DATETIME, 
  `FechaPenPeso` DATETIME, 
  `FechaAntPeso` DATETIME, 
  `PesoNacimiento` FLOAT NULL, 
  `PesoDestete` FLOAT NULL, 
  `PesoIngreso` FLOAT NULL, 
  `PesoUlt` FLOAT NULL, 
  `PesoPen` FLOAT NULL, 
  `PesoAnt` FLOAT NULL, 
  `Peso205A` FLOAT NULL, 
  `Peso365A` FLOAT NULL, 
  `Peso18A` FLOAT NULL, 
  `Peso24A` FLOAT NULL, 
  `Peso205P` FLOAT NULL, 
  `Peso365P` FLOAT NULL, 
  `Peso18P` FLOAT NULL, 
  `Peso24P` FLOAT NULL, 
  `AlturaNacimiento` FLOAT NULL, 
  `AlturaDestete` FLOAT NULL, 
  `AlturaIngreso` FLOAT NULL, 
  `AlturaAnt` FLOAT NULL, 
  `AlturaPen` FLOAT NULL, 
  `AlturaUlt` FLOAT NULL, 
  `Altura205A` FLOAT NULL, 
  `Altura365A` FLOAT NULL, 
  `Altura18A` FLOAT NULL, 
  `Altura24A` FLOAT NULL, 
  `FechaInicial` DATETIME, 
  `PesoInicial` FLOAT NULL, 
  `FechaEntradaServicio` DATETIME, 
  `PesoEntradaServicio` FLOAT NULL, 
  `GananciaEntradaServicio` FLOAT NULL, 
  `AlturaEntradaServicio` FLOAT NULL, 
  `FechaAlParto` DATETIME, 
  `PesoAlParto` FLOAT NULL, 
  `AlturaAlParto` FLOAT NULL, 
  `FechaAlSecado` DATETIME, 
  `PesoAlSecado` FLOAT NULL, 
  `AlturaAlSecado` FLOAT NULL, 
  `FechaALaVenta` DATETIME, 
  `PesoALaVenta` FLOAT NULL, 
  `AlturaALaVenta` FLOAT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Campos_Eventos'
#

DROP TABLE IF EXISTS `tbl_Campos_Eventos`;

CREATE TABLE `tbl_Campos_Eventos` (
  `id_Campo_Evento` INTEGER NOT NULL AUTO_INCREMENT, 
  `des_Campo_Evento` VARCHAR(50), 
  `Convertir` TINYINT(1), 
  `SQL_Convertir` LONGTEXT, 
  INDEX (`id_Campo_Evento`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Crecimiento_1'
#

DROP TABLE IF EXISTS `tbl_Crecimiento_1`;

CREATE TABLE `tbl_Crecimiento_1` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaIngreso` DATETIME, 
  `FechaNacimiento` DATETIME
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Crecimiento_2'
#

DROP TABLE IF EXISTS `tbl_Crecimiento_2`;

CREATE TABLE `tbl_Crecimiento_2` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `FechaIngreso` DATETIME, 
  `FechaNacimiento` DATETIME
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Curva_Crecimiento_TF'
#

DROP TABLE IF EXISTS `tbl_Curva_Crecimiento_TF`;

CREATE TABLE `tbl_Curva_Crecimiento_TF` (
  `Edad` DOUBLE NULL, 
  `Limite_Min` FLOAT NULL, 
  `Limite_Max` FLOAT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Curva_Crecimiento_TG'
#

DROP TABLE IF EXISTS `tbl_Curva_Crecimiento_TG`;

CREATE TABLE `tbl_Curva_Crecimiento_TG` (
  `Edad` DOUBLE NULL, 
  `Limite_Min` FLOAT NULL, 
  `Limite_Max` FLOAT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Especies'
#

DROP TABLE IF EXISTS `tbl_Especies`;

CREATE TABLE `tbl_Especies` (
  `IDEspecie` INTEGER AUTO_INCREMENT, 
  `Descripcion_Especie` VARCHAR(10) NOT NULL, 
  `Dias_Gestacion_Especie` INTEGER NOT NULL DEFAULT 0, 
  `Maxima_Gestacion_Especie` INTEGER NOT NULL DEFAULT 0, 
  `Minima_Gestacion_Especie` INTEGER NOT NULL DEFAULT 0, 
  `Intervalo_Celos_Especie` INTEGER NOT NULL DEFAULT 0, 
  `AHCPA` VARCHAR(12) NOT NULL, 
  `AHSPA` VARCHAR(12) NOT NULL, 
  `DHSPA` VARCHAR(12) NOT NULL, 
  `LHSPA` VARCHAR(12) NOT NULL, 
  `AMREP` VARCHAR(12) NOT NULL, 
  `AMNRP` VARCHAR(12) NOT NULL, 
  `DMNRP` VARCHAR(12) NOT NULL, 
  `LMNRP` VARCHAR(12) NOT NULL, 
  INDEX (`IDEspecie`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Eventos'
#

DROP TABLE IF EXISTS `tbl_Eventos`;

CREATE TABLE `tbl_Eventos` (
  `id_Evento` INTEGER NOT NULL AUTO_INCREMENT, 
  `des_Evento` VARCHAR(50), 
  `Vientres` TINYINT(1), 
  `NoVientres` TINYINT(1), 
  `Toros` TINYINT(1), 
  `Tabla` VARCHAR(50), 
  `Orden_Procesar` TINYINT(3) UNSIGNED DEFAULT 0, 
  INDEX (`id_Evento`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Formato_Importacion'
#

DROP TABLE IF EXISTS `tbl_Formato_Importacion`;

CREATE TABLE `tbl_Formato_Importacion` (
  `id_Formato_Importacion` INTEGER AUTO_INCREMENT, 
  `des_Formato_Importacion` VARCHAR(50), 
  `nom_Hoja` VARCHAR(50) DEFAULT 'Hoja1', 
  `id_Tipo_Codigo` TINYINT(3) UNSIGNED DEFAULT 1, 
  `Ubicacion_Codigo` VARCHAR(2) DEFAULT 'A', 
  `id_Grupo` INTEGER DEFAULT 0, 
  `Campo_Excel` VARCHAR(50), 
  INDEX (`id_Formato_Importacion`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Inventario'
#

DROP TABLE IF EXISTS `tbl_Inventario`;

CREATE TABLE `tbl_Inventario` (
  `id_Inventario` INTEGER NOT NULL AUTO_INCREMENT, 
  `IDDatosGeneralesAnimal` INTEGER NOT NULL DEFAULT 0, 
  `Fecha` DATETIME NOT NULL, 
  `Vaquera` VARCHAR(4), 
  `Observacion` VARCHAR(40), 
  INDEX (`id_Inventario`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Observaciones'
#

DROP TABLE IF EXISTS `tbl_Observaciones`;

CREATE TABLE `tbl_Observaciones` (
  `id_Observacion` INTEGER NOT NULL AUTO_INCREMENT, 
  `IDDatosGeneralesAnimal` INTEGER NOT NULL DEFAULT 0, 
  `Fecha` DATETIME NOT NULL, 
  `Descripción` LONGTEXT, 
  INDEX (`id_Observacion`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_Otras_Variables_Lista'
#

DROP TABLE IF EXISTS `tbl_Otras_Variables_Lista`;

CREATE TABLE `tbl_Otras_Variables_Lista` (
  `id_Lista` INTEGER NOT NULL AUTO_INCREMENT, 
  `id_Variable` INTEGER NOT NULL DEFAULT 0, 
  `Valor_Lista` LONGTEXT NOT NULL, 
  INDEX (`id_Lista`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_rel_Campo_Evento'
#

DROP TABLE IF EXISTS `tbl_rel_Campo_Evento`;

CREATE TABLE `tbl_rel_Campo_Evento` (
  `id_rel_Campo_Evento` INTEGER NOT NULL AUTO_INCREMENT, 
  `id_Evento` INTEGER DEFAULT 0, 
  `id_Campo_Evento` INTEGER DEFAULT 0, 
  `Requerido` TINYINT(1), 
  `Ubicacion_Standar` VARCHAR(2), 
  `Orden` TINYINT(3) UNSIGNED DEFAULT 1, 
  INDEX (`id_rel_Campo_Evento`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'tbl_rel_Campo_Evento_Formatos_Importacion'
#

DROP TABLE IF EXISTS `tbl_rel_Campo_Evento_Formatos_Importacion`;

CREATE TABLE `tbl_rel_Campo_Evento_Formatos_Importacion` (
  `id_rel_Campo_Evento_Formato_Importacion` INTEGER NOT NULL AUTO_INCREMENT, 
  `id_rel_Campo_Evento` INTEGER DEFAULT 0, 
  `id_Formato_Importacion` INTEGER DEFAULT 0, 
  `Ubicacion_Usuario` VARCHAR(2), 
  `Campo_Excel` VARCHAR(50), 
  INDEX (`id_rel_Campo_Evento_Formato_Importacion`)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Tecnicos'
#

DROP TABLE IF EXISTS `Tecnicos`;

CREATE TABLE `Tecnicos` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `NombreTecnico` VARCHAR(40), 
  `Activo` TINYINT(1) DEFAULT 1, 
  `ServiciosTotales` INTEGER DEFAULT 0, 
  `ServiciosPositivos` INTEGER DEFAULT 0, 
  `ServiciosEspera` INTEGER DEFAULT 0, 
  `Preñadas1Servicio` INTEGER DEFAULT 0, 
  `Preñadas2Servicio` INTEGER DEFAULT 0, 
  `Preñadas3Servicio` INTEGER DEFAULT 0, 
  `Preñadas4Servicio` INTEGER DEFAULT 0, 
  `TecnicoComun` TINYINT(1) NOT NULL, 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  `Eliminado` TINYINT(1) DEFAULT 0
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal'
#

DROP TABLE IF EXISTS `Temporal`;

CREATE TABLE `Temporal` (
  `Estado` VARCHAR(10), 
  `Muertes` INTEGER, 
  `Ventas` INTEGER, 
  `Sacrificios` INTEGER, 
  `Compras` INTEGER, 
  `Nacimientos` INTEGER, 
  `Robos` INTEGER, 
  `Donaciones` INTEGER, 
  `APositivos` INTEGER, 
  `ANegativos` INTEGER
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Abortos'
#

DROP TABLE IF EXISTS `Temporal_Abortos`;

CREATE TABLE `Temporal_Abortos` (
  `Med` INTEGER, 
  `Fec` DATETIME, 
  `Pra` VARCHAR(12), 
  `Uni` VARCHAR(12), 
  `Tip` VARCHAR(1), 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Calculo20090112102923'
#

DROP TABLE IF EXISTS `Temporal_Calculo20090112102923`;

CREATE TABLE `Temporal_Calculo20090112102923` (
  `Variable` FLOAT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Movimientos'
#

DROP TABLE IF EXISTS `Temporal_Movimientos`;

CREATE TABLE `Temporal_Movimientos` (
  `Med` INTEGER, 
  `Fec` DATETIME, 
  `Pra` VARCHAR(12), 
  `Tip` VARCHAR(18), 
  `Est` VARCHAR(10), 
  `Ani` INTEGER, 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Partos'
#

DROP TABLE IF EXISTS `Temporal_Partos`;

CREATE TABLE `Temporal_Partos` (
  `Med` INTEGER, 
  `Fec` DATETIME, 
  `Pra` VARCHAR(12), 
  `Uni` VARCHAR(12), 
  `Tip` VARCHAR(10), 
  `TSE` VARCHAR(12), 
  `Tec` VARCHAR(12), 
  `Sexo` VARCHAR(13), 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Pesajes'
#

DROP TABLE IF EXISTS `Temporal_Pesajes`;

CREATE TABLE `Temporal_Pesajes` (
  `Med` INTEGER, 
  `Fec` DATETIME, 
  `Pra` VARCHAR(12), 
  `Uni` VARCHAR(12), 
  `Pe1` FLOAT NULL, 
  `Pe2` FLOAT NULL, 
  `Pe3` FLOAT NULL, 
  `PeT` FLOAT NULL, 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Pesos_Corporales'
#

DROP TABLE IF EXISTS `Temporal_Pesos_Corporales`;

CREATE TABLE `Temporal_Pesos_Corporales` (
  `Med` INTEGER, 
  `Fec` DATETIME, 
  `Pra` VARCHAR(12), 
  `Uni` VARCHAR(12), 
  `Tip` VARCHAR(10), 
  `Pes` FLOAT NULL, 
  `Alt` FLOAT NULL, 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Resumen_NV'
#

DROP TABLE IF EXISTS `Temporal_Resumen_NV`;

CREATE TABLE `Temporal_Resumen_NV` (
  `TBecerros` INTEGER, 
  `TBecerras` INTEGER, 
  `TMautes` INTEGER, 
  `TMautas` INTEGER, 
  `TNovillos` INTEGER, 
  `TAnimales` INTEGER, 
  `EBoNo` INTEGER, 
  `EBaNo` INTEGER, 
  `EMeNo` INTEGER, 
  `EMaNo` INTEGER, 
  `ENoNo` INTEGER, 
  `EAnNo` INTEGER, 
  `EBoProm` FLOAT NULL, 
  `EBaProm` FLOAT NULL, 
  `EMeProm` FLOAT NULL, 
  `EMaProm` FLOAT NULL, 
  `ENoProm` FLOAT NULL, 
  `EAnProm` FLOAT NULL, 
  `MPNacNo` INTEGER, 
  `MPDesNo` INTEGER, 
  `MP205No` INTEGER, 
  `MP365No` INTEGER, 
  `MP18MNo` INTEGER, 
  `MP24MNo` INTEGER, 
  `MPNacProm` INTEGER, 
  `MPDesProm` INTEGER, 
  `MP205Prom` INTEGER, 
  `MP365Prom` INTEGER, 
  `MP18MProm` INTEGER, 
  `MP24MProm` INTEGER, 
  `HPNacNo` INTEGER, 
  `HPDesNo` INTEGER, 
  `HP205No` INTEGER, 
  `HP365No` INTEGER, 
  `HP18MNo` INTEGER, 
  `HP24MNo` INTEGER, 
  `HPNacProm` FLOAT NULL, 
  `HPDesProm` FLOAT NULL, 
  `HP205Prom` FLOAT NULL, 
  `HP365Prom` FLOAT NULL, 
  `HP18MProm` FLOAT NULL, 
  `HP24MProm` FLOAT NULL, 
  `PBoNo` INTEGER, 
  `PBaNo` INTEGER, 
  `PMeNo` INTEGER, 
  `PMaNo` INTEGER, 
  `PNoNo` INTEGER, 
  `PAnNo` INTEGER, 
  `PBoProm` FLOAT NULL, 
  `PBaProm` FLOAT NULL, 
  `PMeProm` FLOAT NULL, 
  `PMaProm` FLOAT NULL, 
  `PNoProm` FLOAT NULL, 
  `PAnProm` FLOAT NULL, 
  `GPBoNo` INTEGER, 
  `GPBaNo` INTEGER, 
  `GPMeNo` INTEGER, 
  `GPMaNo` INTEGER, 
  `GPNoNo` INTEGER, 
  `GPAnNo` INTEGER, 
  `GPBoProm` FLOAT NULL, 
  `GPBaProm` FLOAT NULL, 
  `GPMeProm` FLOAT NULL, 
  `GPMaProm` FLOAT NULL, 
  `GPNoProm` FLOAT NULL, 
  `GPAnProm` FLOAT NULL, 
  `GTBoNo` INTEGER, 
  `GTBaNo` INTEGER, 
  `GTMeNo` INTEGER, 
  `GTMaNo` INTEGER, 
  `GTNoNo` INTEGER, 
  `GTAnNo` INTEGER, 
  `GTBoProm` FLOAT NULL, 
  `GTBaProm` FLOAT NULL, 
  `GTMeProm` FLOAT NULL, 
  `GTMaProm` FLOAT NULL, 
  `GTNoProm` FLOAT NULL, 
  `GTAnProm` FLOAT NULL, 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Resumen_Vientres'
#

DROP TABLE IF EXISTS `Temporal_Resumen_Vientres`;

CREATE TABLE `Temporal_Resumen_Vientres` (
  `TNovillas` INTEGER, 
  `TVacas` INTEGER, 
  `TAnimales` INTEGER, 
  `EdadNo` INTEGER, 
  `EdadProm` FLOAT NULL, 
  `TDescartados` INTEGER, 
  `PartosNo` INTEGER, 
  `PartosProm` FLOAT NULL, 
  `AbortosNo` INTEGER, 
  `AbortosProm` FLOAT NULL, 
  `LactanciasNo` INTEGER, 
  `LactanciasProm` FLOAT NULL, 
  `NovillasSinServicio` INTEGER, 
  `NovillasVaciasConServicio` INTEGER, 
  `NovillasPreñadas` INTEGER, 
  `NovillasEnEspera` INTEGER, 
  `VacasSinServicio` INTEGER, 
  `VacasVaciasConServicio` INTEGER, 
  `VacasPreñadas` INTEGER, 
  `VacasEnEspera` INTEGER, 
  `InseminNo` INTEGER, 
  `MontasNo` INTEGER, 
  `DiasVaciaNo` INTEGER, 
  `DiasServidaNo` INTEGER, 
  `DiasParidaNo` INTEGER, 
  `IndeminProm` FLOAT NULL, 
  `MontasProm` FLOAT NULL, 
  `DiasVaciaProm` FLOAT NULL, 
  `DiasServidaProm` FLOAT NULL, 
  `DiasParidaProm` FLOAT NULL, 
  `DiasProdNo` INTEGER, 
  `ProdTotNo` INTEGER, 
  `Prod244No` INTEGER, 
  `Prod305No` INTEGER, 
  `PromDiaActNo` INTEGER, 
  `DiasSecaActNo` INTEGER, 
  `PromUltPesajeNo` INTEGER, 
  `DiasProdProm` FLOAT NULL, 
  `ProdTotProm` FLOAT NULL, 
  `Prod244Prom` FLOAT NULL, 
  `Prod305Prom` FLOAT NULL, 
  `PromDiaActProm` FLOAT NULL, 
  `DiasSecaAcProm` FLOAT NULL, 
  `PromUltPesajeProm` FLOAT NULL, 
  `PrmPartoPartoNo` INTEGER, 
  `PromProdxPartoNo` INTEGER, 
  `PromDiarioLactNo` INTEGER, 
  `PromGanxPartoNo` INTEGER, 
  `PrmPartoPartoProm` FLOAT NULL, 
  `PromProdxPartoProm` FLOAT NULL, 
  `PromDiarioLactProm` FLOAT NULL, 
  `PromGanxPartoProm` FLOAT NULL, 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Revision'
#

DROP TABLE IF EXISTS `Temporal_Revision`;

CREATE TABLE `Temporal_Revision` (
  `Med` INTEGER, 
  `Fec` DATETIME, 
  `Pra` VARCHAR(12), 
  `Uni` VARCHAR(12), 
  `Rev` VARCHAR(4), 
  `Dia` VARCHAR(4), 
  `Tra` VARCHAR(4), 
  `DPR` INTEGER, 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Secados_Destetes'
#

DROP TABLE IF EXISTS `Temporal_Secados_Destetes`;

CREATE TABLE `Temporal_Secados_Destetes` (
  `Med` INTEGER, 
  `Fec` DATETIME, 
  `Pra` VARCHAR(12), 
  `Uni` VARCHAR(12), 
  `Pe1` FLOAT NULL, 
  `Pe2` FLOAT NULL, 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Temporal_Servicios'
#

DROP TABLE IF EXISTS `Temporal_Servicios`;

CREATE TABLE `Temporal_Servicios` (
  `Med` INTEGER, 
  `Fec` DATETIME, 
  `Pra` VARCHAR(12), 
  `Uni` VARCHAR(12), 
  `Tip` VARCHAR(10), 
  `TSE` VARCHAR(12), 
  `Tec` VARCHAR(12), 
  `Dos` FLOAT NULL, 
  `Hor` VARCHAR(2), 
  `ID_Usuario` INTEGER, 
  `Fecha_Sesion` VARCHAR(10)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'TemporalAnimalesPorVaquera'
#

DROP TABLE IF EXISTS `TemporalAnimalesPorVaquera`;

CREATE TABLE `TemporalAnimalesPorVaquera` (
  `CodigoUltCambioV` VARCHAR(4), 
  `Becerra` VARCHAR(255), 
  `Becerro` VARCHAR(255), 
  `Maute` VARCHAR(255), 
  `Mauta` VARCHAR(255), 
  `Novillo` LONGBLOB, 
  `Novilla` VARCHAR(255), 
  `Toro` VARCHAR(255), 
  `Vaca` VARCHAR(255), 
  `Total` INTEGER, 
  `Descripcion` VARCHAR(255)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'TemporalComposicion20081211110451'
#

DROP TABLE IF EXISTS `TemporalComposicion20081211110451`;

CREATE TABLE `TemporalComposicion20081211110451` (
  `ID` INTEGER NOT NULL, 
  `UnicoPadre` VARCHAR(12), 
  `UnicoMadre` VARCHAR(12)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'TemporalPesajes12Meses'
#

DROP TABLE IF EXISTS `TemporalPesajes12Meses`;

CREATE TABLE `TemporalPesajes12Meses` (
  `CodHa` VARCHAR(6) NOT NULL DEFAULT '', 
  `Unico` VARCHAR(12) NOT NULL DEFAULT '', 
  `Practico` VARCHAR(12) NOT NULL DEFAULT '', 
  `Vaquera` VARCHAR(4) NOT NULL DEFAULT '', 
  `PesoMes1` FLOAT NULL DEFAULT 0, 
  `PesoMes2` FLOAT NULL DEFAULT 0, 
  `PesoMes3` FLOAT NULL DEFAULT 0, 
  `PesoMes4` FLOAT NULL DEFAULT 0, 
  `PesoMes5` FLOAT NULL DEFAULT 0, 
  `PesoMes6` FLOAT NULL DEFAULT 0, 
  `PesoMes7` FLOAT NULL DEFAULT 0, 
  `PesoMes8` FLOAT NULL DEFAULT 0, 
  `PesoMes9` FLOAT NULL DEFAULT 0, 
  `PesoMes10` FLOAT NULL DEFAULT 0, 
  `PesoMes11` FLOAT NULL DEFAULT 0, 
  `PesoMes12` FLOAT NULL DEFAULT 0
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'TemporalServicios'
#

DROP TABLE IF EXISTS `TemporalServicios`;

CREATE TABLE `TemporalServicios` (
  `Fec` DATETIME, 
  `Pra` VARCHAR(12), 
  `Uni` VARCHAR(12), 
  `Tip` VARCHAR(10), 
  `TSE` VARCHAR(12), 
  `Tec` VARCHAR(12), 
  `Dos` FLOAT NULL, 
  `Hor` VARCHAR(2)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'TorosSemenEmbrion'
#

DROP TABLE IF EXISTS `TorosSemenEmbrion`;

CREATE TABLE `TorosSemenEmbrion` (
  `CodHa` VARCHAR(6) NOT NULL, 
  `Unico` VARCHAR(12) NOT NULL, 
  `Tipo` VARCHAR(10) NOT NULL, 
  `Activo` TINYINT(1), 
  `NumeroUnidades` FLOAT NULL DEFAULT 0, 
  `MachosNacidos` INTEGER DEFAULT 0, 
  `HembrasNacidas` INTEGER DEFAULT 0, 
  `ServiciosTotales` INTEGER DEFAULT 0, 
  `ServiciosPositivos` INTEGER DEFAULT 0, 
  `ServiciosEspera` INTEGER DEFAULT 0, 
  `Preñadas1Servicio` INTEGER DEFAULT 0, 
  `Preñadas2Servicio` INTEGER DEFAULT 0, 
  `Preñadas3Servicio` INTEGER DEFAULT 0, 
  `Preñadas4Servicio` INTEGER DEFAULT 0, 
  `EdadEmbrion` INTEGER
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'Usuarios'
#

DROP TABLE IF EXISTS `Usuarios`;

CREATE TABLE `Usuarios` (
  `CodUsuario` VARCHAR(8) NOT NULL DEFAULT '', 
  `Usuario` VARCHAR(30) NOT NULL, 
  `Clave` VARCHAR(8) NOT NULL DEFAULT '', 
  `NivelAcceso` INTEGER NOT NULL
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'VariablesCierres'
#

DROP TABLE IF EXISTS `VariablesCierres`;

CREATE TABLE `VariablesCierres` (
  `Orden` INTEGER, 
  `Descripcion` VARCHAR(40), 
  `Tipo` INTEGER NOT NULL, 
  `Campo1` VARCHAR(20), 
  `Campo2` VARCHAR(20), 
  `Campo3` VARCHAR(20), 
  `Campo4` VARCHAR(20), 
  `Campo5` VARCHAR(20), 
  `Campo6` VARCHAR(20), 
  `Campo7` VARCHAR(20), 
  `Campo8` VARCHAR(20), 
  `Campo9` VARCHAR(20), 
  `Campo10` VARCHAR(20), 
  `Campo11` VARCHAR(20), 
  `Campo12` VARCHAR(20), 
  `Campo13` VARCHAR(20), 
  `Campo14` VARCHAR(20), 
  `Campo15` VARCHAR(20), 
  `Campo16` VARCHAR(20), 
  `Campo17` VARCHAR(20), 
  `Campo18` VARCHAR(20), 
  `Campo19` VARCHAR(20), 
  `Campo20` VARCHAR(20), 
  `G1` TINYINT(1) NOT NULL, 
  `G2` TINYINT(1) NOT NULL, 
  `G3` TINYINT(1) NOT NULL, 
  `G4` TINYINT(1) NOT NULL, 
  `G5` TINYINT(1) NOT NULL, 
  `G6` TINYINT(1) NOT NULL, 
  `G7` TINYINT(1) NOT NULL, 
  `G8` TINYINT(1) NOT NULL, 
  `G9` TINYINT(1) NOT NULL, 
  `G10` TINYINT(1) NOT NULL, 
  `G11` TINYINT(1) NOT NULL, 
  `G12` TINYINT(1) NOT NULL, 
  `G13` TINYINT(1) NOT NULL, 
  `G14` TINYINT(1) NOT NULL, 
  `G15` TINYINT(1) NOT NULL, 
  `G16` TINYINT(1) NOT NULL, 
  `G17` TINYINT(1) NOT NULL, 
  `G18` TINYINT(1) NOT NULL, 
  `G19` TINYINT(1) NOT NULL, 
  `G20` TINYINT(1) NOT NULL, 
  `G21` TINYINT(1) NOT NULL, 
  `G22` TINYINT(1) NOT NULL, 
  `G23` TINYINT(1) NOT NULL, 
  `G24` TINYINT(1) NOT NULL, 
  `G25` TINYINT(1) NOT NULL, 
  `G26` TINYINT(1) NOT NULL, 
  `G27` TINYINT(1) NOT NULL, 
  `G28` TINYINT(1) NOT NULL, 
  `G29` TINYINT(1) NOT NULL, 
  `G30` TINYINT(1) NOT NULL, 
  `G31` TINYINT(1) NOT NULL, 
  `TG` TINYINT(1)
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'VariablesImprimirReportes'
#

DROP TABLE IF EXISTS `VariablesImprimirReportes`;

CREATE TABLE `VariablesImprimirReportes` (
  `IdRep` INTEGER NOT NULL, 
  `CodVarRep` VARCHAR(6) NOT NULL, 
  `Posicion` INTEGER NOT NULL, 
  `TotalizarResumen` TINYINT(1), 
  `ModificadoPor` VARCHAR(20) NOT NULL, 
  `Ancho` INTEGER
) ENGINE=myisam DEFAULT CHARSET=utf8;

#
# Table structure for table 'VariablesReportes'
#

DROP TABLE IF EXISTS `VariablesReportes`;

CREATE TABLE `VariablesReportes` (
  `CodVarRep` VARCHAR(6) NOT NULL, 
  `DesVarRep` VARCHAR(60) NOT NULL, 
  `GV1` TINYINT(1), 
  `GV2` TINYINT(1), 
  `GV3` TINYINT(1), 
  `GV4` TINYINT(1), 
  `GV5` TINYINT(1), 
  `GV6` TINYINT(1), 
  `GV7` TINYINT(1), 
  `GV8` TINYINT(1), 
  `GV9` TINYINT(1), 
  `GV10` TINYINT(1), 
  `GVE` TINYINT(1), 
  `Titulo1` VARCHAR(20), 
  `Titulo2` VARCHAR(20), 
  `Decimales` INTEGER DEFAULT 0, 
  `TablaOrigen` VARCHAR(50), 
  `NombreVarRep` VARCHAR(50), 
  `TipoCalculo` INTEGER NOT NULL, 
  `TipoVariable` INTEGER NOT NULL, 
  `Activa` TINYINT(1), 
  `Var1` VARCHAR(60), 
  `Var2` VARCHAR(60), 
  `Var3` VARCHAR(60), 
  `Var4` VARCHAR(60), 
  `TablaVinculada` VARCHAR(50), 
  `CampoClave` VARCHAR(50), 
  `AliasTablaVinculada` VARCHAR(50), 
  `Vientres` TINYINT(1), 
  `NoVientres` TINYINT(1), 
  `Toros` TINYINT(1), 
  `Tecnicos` TINYINT(1), 
  `Hacienda` TINYINT(1), 
  `PDA` TINYINT(1), 
  `PDAObligatorio` TINYINT(1), 
  `PDAVista` VARCHAR(20), 
  `PDAConfiguracionFinal` TINYINT(1), 
  `PDATablaDestino` VARCHAR(20), 
  `DesCortaVarRep` VARCHAR(20)
) ENGINE=myisam DEFAULT CHARSET=utf8;

