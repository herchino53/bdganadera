-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 07-07-2023 a las 22:08:15
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ganaderadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2023_05_17_174800_create_tbl_animales', 1),
(2, '2023_05_17_175637_create_tbl_criadores', 1),
(3, '2023_05_17_175832_create_tbl_fincas', 1),
(4, '2023_05_17_180324_create_tbl_grupo_fisiologico', 1),
(5, '2023_05_17_180343_create_tbl_propietarios', 1),
(6, '2023_05_17_180553_create_tbl_veterinarios', 1),
(7, '2023_05_17_180636_create_tbr_palpacion_rectal', 1),
(8, '2023_05_17_180701_create_tbr_produccion_lactea', 1),
(9, '2023_05_17_180724_create_tbr_revision_ginecologica', 1),
(10, '2023_05_17_180741_create_tbr_vacunacion', 1),
(11, '2023_05_17_200522_create_tbr_arbol_genealogico', 1),
(12, '2023_05_24_153129_create_tbl_user_types', 1),
(13, '2023_05_24_153140_create_tbl_user', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_animales`
--

DROP TABLE IF EXISTS `tbl_animales`;
CREATE TABLE IF NOT EXISTS `tbl_animales` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo_reg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nacimiento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peso_nacer` int(11) NOT NULL,
  `peso_destete` int(11) NOT NULL,
  `peso_18_meses` int(11) NOT NULL,
  `peso_primer_parto` int(11) NOT NULL,
  `id_finca` int(10) UNSIGNED DEFAULT NULL,
  `id_criadores` int(10) UNSIGNED DEFAULT NULL,
  `id_grupo_fisiologico` int(10) UNSIGNED DEFAULT NULL,
  `estado` int(5) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_animales_id_finca_foreign` (`id_finca`),
  KEY `tbl_animales_id_criadores_foreign` (`id_criadores`),
  KEY `tbl_animales_id_grupo_fisiologico_foreign` (`id_grupo_fisiologico`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_animales`
--

INSERT INTO `tbl_animales` (`id`, `codigo_reg`, `nombre`, `sexo`, `fecha_nacimiento`, `color`, `peso_nacer`, `peso_destete`, `peso_18_meses`, `peso_primer_parto`, `id_finca`, `id_criadores`, `id_grupo_fisiologico`, `estado`, `created_at`, `updated_at`) VALUES
(1, '1', 'Mariposa', 'Hembra', '01-01-2020', 'blanca', 20, 40, 50, 100, 1, 1, 7, 0, NULL, '2023-06-26 04:00:00'),
(2, '2', 'Mariposon', 'Macho', '01-01-2020', 'negro', 20, 25, 30, 35, 1, 1, 8, 0, '2023-06-21 04:00:00', '2023-06-26 04:00:00'),
(3, '23', 'Palomo', 'Macho', '11-11-2020', 'blanco', 20, 23, 25, 30, 2, 1, 8, 1, '2023-07-04 04:00:00', NULL),
(4, '123', 'palomino', 'masculino', '01-06-2020', 'negro', 10, 15, 20, 20, 1, 1, 8, 1, '2023-07-06 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_criadores`
--

DROP TABLE IF EXISTS `tbl_criadores`;
CREATE TABLE IF NOT EXISTS `tbl_criadores` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cedula` int(11) NOT NULL,
  `telefono` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_criadores`
--

INSERT INTO `tbl_criadores` (`id`, `nombre`, `apellido`, `cedula`, `telefono`, `correo`, `created_at`, `updated_at`) VALUES
(1, 'Juan', 'Parranda', 6543012, '04129876655', 'mamagallo@gmail.com', NULL, '2023-06-09 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_fincas`
--

DROP TABLE IF EXISTS `tbl_fincas`;
CREATE TABLE IF NOT EXISTS `tbl_fincas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo_reg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_propietario` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_fincas_id_propietario_foreign` (`id_propietario`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_fincas`
--

INSERT INTO `tbl_fincas` (`id`, `codigo_reg`, `nombre`, `direccion`, `telefono`, `id_propietario`, `created_at`, `updated_at`) VALUES
(1, '1', 'finca de melano', 'paso la puya', '04141234572', 1, NULL, '2023-06-09 04:00:00'),
(2, '2', 'Finca Brava', 'avenida del monte', '04141114455', 1, '2023-06-12 04:00:00', '2023-06-26 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_grupo_fisiologico`
--

DROP TABLE IF EXISTS `tbl_grupo_fisiologico`;
CREATE TABLE IF NOT EXISTS `tbl_grupo_fisiologico` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_grupo_fisiologico`
--

INSERT INTO `tbl_grupo_fisiologico` (`id`, `nombre`, `sexo`, `edad`, `created_at`, `updated_at`) VALUES
(1, 'Ternero', 'Macho', 1, '2023-06-05 23:58:20', '2023-06-26 04:00:00'),
(2, 'Novillo', 'Macho', 2, '2023-06-05 23:58:20', '2023-06-26 04:00:00'),
(3, 'Ternera', 'Hembra', 2, '2023-06-05 23:58:20', '2023-06-26 04:00:00'),
(4, 'Becerro', 'Macho', 1, '2023-06-05 23:58:20', '2023-06-26 04:00:00'),
(5, 'Maute', 'Macho', 3, '2023-06-05 23:58:20', '2023-06-26 04:00:00'),
(6, 'Mauta', 'Hembra', 3, '2023-06-05 23:58:20', '2023-06-26 04:00:00'),
(7, 'Vaca', 'Hembra', 4, '2023-06-05 23:58:20', '2023-06-26 04:00:00'),
(8, 'Toro', 'Macho', 4, '2023-06-05 23:58:20', '2023-06-26 04:00:00'),
(9, 'Toretes', 'Macho', 3, '2023-06-05 23:58:20', '2023-06-26 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_propietarios`
--

DROP TABLE IF EXISTS `tbl_propietarios`;
CREATE TABLE IF NOT EXISTS `tbl_propietarios` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cedula` int(11) NOT NULL,
  `telefono` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_propietarios`
--

INSERT INTO `tbl_propietarios` (`id`, `nombre`, `apellido`, `cedula`, `telefono`, `correo`, `created_at`, `updated_at`) VALUES
(1, 'Rosa Carolina', 'Melano', 12455654, '04141234567', 'rosamelano@gmail.com', NULL, '2023-06-26 04:00:00'),
(2, 'Rosa Petrolina', 'Melannito', 12455654, '04141234567', 'rosamelano@gmail.com', NULL, '2023-06-26 04:00:00'),
(3, 'Carlos ', 'Perez', 987456, '04149875566', 'carlosperez@gmail.com', '2023-06-26 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perfil` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type_id` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_user_user_type_id_foreign` (`user_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `nombre`, `apellido`, `name`, `username`, `perfil`, `ci`, `email`, `email_verified_at`, `password`, `user_type_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hernán ', 'Abreu', 'Hernán Abreu', 'djherchino', 'Admin', 16576717, 'herchino53@gmail.com', '', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 1, NULL, '2023-06-06 00:20:32', '2023-07-06 04:00:00'),
(2, 'Ingrid Patricia', 'Abreu', 'Ingrid Abreu', 'ipal', 'Coor', 17475463, 'ipal16@hotmail.com', '', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 2, NULL, '2023-06-06 00:34:12', '2023-07-06 04:00:00'),
(3, 'Hernan J', 'Abreu L', 'Hernán José', 'hernan', 'Sup', 16555999, 'herchino531@gmail.com', '', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 3, NULL, '2023-06-06 00:56:04', '2023-07-07 04:00:00'),
(4, 'Ingrid Yadira', 'Liendo Freites', 'yadira', 'yadira', 'Vet', 6256104, 'ingriliendo@gmail.com', 'ingriliendo@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 4, NULL, '2023-06-06 00:57:49', '2023-07-07 04:00:00'),
(5, 'Chino', 'Miranda', 'chino miranda', 'chino', 'Aux', 12312312, 'herchino153@gmail.com', '', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 5, NULL, '2023-06-06 01:00:22', '2023-07-07 04:00:00'),
(6, 'Hernan Jose', 'Abreu Diaz', 'Hernan Jose Abreu Diaz', 'patricio', NULL, 541097, 'patricio.charal@gmail.com', 'patricio.charal@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 6, NULL, '2023-06-28 04:00:00', '2023-07-07 04:00:00'),
(10, 'Juan Antonio', 'Gonzalez', 'Juan Gonzalez', 'juango', NULL, 963258, 'juango@gmail.com', 'juango@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 4, NULL, '2023-07-06 04:00:00', '2023-07-07 04:00:00'),
(9, 'Juan de Jesus', 'Diaz', 'juan ', 'juan', NULL, 123456, 'juanhilario@gmail.com', 'juanhilario@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 6, NULL, '2023-06-28 04:00:00', '2023-07-07 04:00:00'),
(14, 'jose ', 'tovar', 'josetovar', 'josetovar', NULL, 123123, 'jose@gmail.com', 'jose@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 4, NULL, '2023-07-07 04:00:00', '2023-07-07 04:00:00'),
(12, 'veterinario', 'veterinario', 'veterinario', 'veterinario', NULL, 5434334, 'veterinario@gmail.com', 'veterinario@gmail.com', '$2a$07$asxx54ahjppf45sd87a5aub7LdtrTXnn.ZQdALsthndsluPeTbv.a', 4, NULL, '2023-07-07 04:00:00', '2023-07-07 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_user_types`
--

DROP TABLE IF EXISTS `tbl_user_types`;
CREATE TABLE IF NOT EXISTS `tbl_user_types` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_user_types`
--

INSERT INTO `tbl_user_types` (`id`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', '2023-06-06 00:20:32', '2023-06-06 00:20:32'),
(2, 'Coordinador', '2023-06-06 00:20:32', '2023-06-06 00:20:32'),
(3, 'Supervisor', '2023-06-06 00:20:32', '2023-06-06 00:20:32'),
(4, 'Veterinario', '2023-06-06 00:20:32', '2023-06-06 00:20:32'),
(5, 'Auxiliar', '2023-06-06 00:20:32', '2023-06-06 00:20:32'),
(6, 'Propietario', '2023-06-06 00:20:32', '2023-06-06 00:20:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_veterinarios`
--

DROP TABLE IF EXISTS `tbl_veterinarios`;
CREATE TABLE IF NOT EXISTS `tbl_veterinarios` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo_reg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cedula` int(11) NOT NULL,
  `telefono` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbl_veterinarios`
--

INSERT INTO `tbl_veterinarios` (`id`, `codigo_reg`, `nombre`, `apellido`, `cedula`, `telefono`, `correo`, `created_at`, `updated_at`) VALUES
(1, '234', 'Juan', 'Gonzalez', 963258, '04144566655', 'juangonzalez@gmail.com', NULL, NULL),
(2, '21', 'hernan1', 'abreu1', 16555999, '04129876655', 'herchino5311@gmail.com', '2023-07-06 04:00:00', NULL),
(3, '645', 'sdfg', 'sdfg', 234234, '', 'sdfsd@hfidf.com', '2023-07-07 04:00:00', NULL),
(4, '000', 'veterinario', 'veterinario', 5434334, '000', 'veterinario@gmail.com', '2023-07-07 04:00:00', '2023-07-07 04:00:00'),
(7, '000', 'jose ', 'tovar', 123123, '000', 'jose@gmail.com', '2023-07-07 04:00:00', '2023-07-07 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbr_arbol_genealogico`
--

DROP TABLE IF EXISTS `tbr_arbol_genealogico`;
CREATE TABLE IF NOT EXISTS `tbr_arbol_genealogico` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo_reg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_animal` int(10) UNSIGNED DEFAULT NULL,
  `id_padre` int(10) UNSIGNED DEFAULT NULL,
  `id_madre` int(10) UNSIGNED DEFAULT NULL,
  `id_abuelo_paterno` int(10) UNSIGNED DEFAULT NULL,
  `id_abuela_paterna` int(10) UNSIGNED DEFAULT NULL,
  `id_abuelo_materna` int(10) UNSIGNED DEFAULT NULL,
  `id_abuela_materna` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbr_arbol_genealogico_id_animal_foreign` (`id_animal`),
  KEY `tbr_arbol_genealogico_id_padre_foreign` (`id_padre`),
  KEY `tbr_arbol_genealogico_id_madre_foreign` (`id_madre`),
  KEY `tbr_arbol_genealogico_id_abuelo_paterno_foreign` (`id_abuelo_paterno`),
  KEY `tbr_arbol_genealogico_id_abuela_paterna_foreign` (`id_abuela_paterna`),
  KEY `tbr_arbol_genealogico_id_abuelo_materna_foreign` (`id_abuelo_materna`),
  KEY `tbr_arbol_genealogico_id_abuela_materna_foreign` (`id_abuela_materna`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbr_arbol_genealogico`
--

INSERT INTO `tbr_arbol_genealogico` (`id`, `codigo_reg`, `id_animal`, `id_padre`, `id_madre`, `id_abuelo_paterno`, `id_abuela_paterna`, `id_abuelo_materna`, `id_abuela_materna`, `created_at`, `updated_at`) VALUES
(1, '01', 1, 2, 1, 1, 2, 2, 1, '2023-06-21 04:00:00', '2023-06-21 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbr_palpacion_rectal`
--

DROP TABLE IF EXISTS `tbr_palpacion_rectal`;
CREATE TABLE IF NOT EXISTS `tbr_palpacion_rectal` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `observacion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `id_animal` int(10) UNSIGNED DEFAULT NULL,
  `id_veterinario` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbr_palpacion_rectal_id_animal_foreign` (`id_animal`),
  KEY `tbr_palpacion_rectal_id_veterinario_foreign` (`id_veterinario`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbr_palpacion_rectal`
--

INSERT INTO `tbr_palpacion_rectal` (`id`, `observacion`, `fecha`, `id_animal`, `id_veterinario`, `created_at`, `updated_at`) VALUES
(1, 'sin observacion', '2023-06-21', 1, 1, '2023-06-21 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbr_produccion_lactea`
--

DROP TABLE IF EXISTS `tbr_produccion_lactea`;
CREATE TABLE IF NOT EXISTS `tbr_produccion_lactea` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo_reg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `litros` int(11) NOT NULL,
  `fecha_registro` date NOT NULL,
  `id_animal` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbr_produccion_lactea_id_animal_foreign` (`id_animal`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbr_produccion_lactea`
--

INSERT INTO `tbr_produccion_lactea` (`id`, `codigo_reg`, `litros`, `fecha_registro`, `id_animal`, `created_at`, `updated_at`) VALUES
(1, '1', 10, '2023-06-21', 1, '2023-06-21 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbr_revision_ginecologica`
--

DROP TABLE IF EXISTS `tbr_revision_ginecologica`;
CREATE TABLE IF NOT EXISTS `tbr_revision_ginecologica` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `diagnostico` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `receta_medica` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `id_animal` int(10) UNSIGNED DEFAULT NULL,
  `id_veterinario` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbr_revision_ginecologica_id_animal_foreign` (`id_animal`),
  KEY `tbr_revision_ginecologica_id_veterinario_foreign` (`id_veterinario`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbr_revision_ginecologica`
--

INSERT INTO `tbr_revision_ginecologica` (`id`, `diagnostico`, `observacion`, `receta_medica`, `fecha`, `id_animal`, `id_veterinario`, `created_at`, `updated_at`) VALUES
(1, 'sin diagnostico', 'sin observaciones', 'sin receta', '2023-06-21', 1, 1, '2023-06-21 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbr_vacunacion`
--

DROP TABLE IF EXISTS `tbr_vacunacion`;
CREATE TABLE IF NOT EXISTS `tbr_vacunacion` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `principio_activo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dosis` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `id_animal` int(10) UNSIGNED DEFAULT NULL,
  `id_veterinario` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbr_vacunacion_id_animal_foreign` (`id_animal`),
  KEY `tbr_vacunacion_id_veterinario_foreign` (`id_veterinario`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tbr_vacunacion`
--

INSERT INTO `tbr_vacunacion` (`id`, `nombre`, `principio_activo`, `marca`, `tipo`, `dosis`, `fecha`, `id_animal`, `id_veterinario`, `created_at`, `updated_at`) VALUES
(1, 'AFTOGAN', 'AFTOGAN', 'VECOL', 'Intramuscular', '2ml', '2023-06-08 00:00:00', 1, 1, NULL, '2023-06-09 04:00:00'),
(2, 'AFTOAN + RABICA', 'AFTOGAN + RABICA', 'VECOL', 'intramuscular', '2ml', '2023-06-08 00:00:00', 2, 1, NULL, '2023-06-09 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
