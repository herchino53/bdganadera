ALTER TABLE tbl_animales ADD FOREIGN KEY (id_finca) REFERENCES tbl_fincas(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbl_animales ADD FOREIGN KEY (id_criadores) REFERENCES tbl_criadores(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbl_animales ADD FOREIGN KEY (id_grupo_fisiologico) REFERENCES tbl_grupo_fisiologico(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE tbl_fincas ADD FOREIGN KEY (id_propietario) REFERENCES tbl_propietarios(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE tbl_user ADD FOREIGN KEY (user_type_id) REFERENCES tbl_user_types(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE tbr_arbol_genealogico ADD FOREIGN KEY (id_animal) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbr_arbol_genealogico ADD FOREIGN KEY (id_padre) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbr_arbol_genealogico ADD FOREIGN KEY (id_madre) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbr_arbol_genealogico ADD FOREIGN KEY (id_abuelo_paterno) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbr_arbol_genealogico ADD FOREIGN KEY (id_abuela_paterna) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbr_arbol_genealogico ADD FOREIGN KEY (id_abuelo_materna) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbr_arbol_genealogico ADD FOREIGN KEY (id_abuela_materna) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE tbr_palpacion_rectal ADD FOREIGN KEY (id_animal) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbr_palpacion_rectal ADD FOREIGN KEY (id_veterinario) REFERENCES tbl_veterinarios(id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE tbr_produccion_lactea ADD FOREIGN KEY (id_animal) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE tbr_revision_ginecologica ADD FOREIGN KEY (id_animal) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbr_revision_ginecologica ADD FOREIGN KEY (id_veterinario) REFERENCES tbl_veterinarios(id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE tbr_vacunacion ADD FOREIGN KEY (id_animal) REFERENCES tbl_animales(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tbr_vacunacion ADD FOREIGN KEY (id_veterinario) REFERENCES tbl_veterinarios(id) ON DELETE CASCADE ON UPDATE CASCADE;
